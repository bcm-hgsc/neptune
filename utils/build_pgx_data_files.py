import pickle


pgx_drug_recommendations = {
    "CYP2C19": [
        {
            "drug": "clopidogrel",
            "id": "C0070166",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/",
        },
        {
            "drug": "voriconazole",
            "id": "C0393080",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/",
        },
        {
            "drug": "citalopram",
            "id": "C0008845",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/",
        },
        {
            "drug": "escitalopram",
            "id": "C1099456",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/",
        },
        {
            "drug": "amitriptyline",
            "id": "C0002600",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/",
        },
    ],
    "DPYD": [
        {
            "drug": "capecitabine",
            "id": "C0671970",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/",
        },
        {
            "drug": "fluorouracil",
            "id": "C0016360",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/",
        },
        {
            "drug": "tegafur",
            "id": "C0016778",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/",
        },
    ],
    "IFNL3": [
        {
            "drug": "peginterferon alfa-2a",
            "id": "C0391001",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/",
        },
        {
            "drug": "peginterferon alfa-2b",
            "id": "C0796545",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/",
        },
        {
            "drug": "ribavirin",
            "id": "C0035525",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/",
        },
    ],
    "SLCO1B1": [
        {
            "drug": "simvastatin",
            "id": "C0074554",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/",
        }
    ],
    "TPMT": [
        {
            "drug": "azathioprine",
            "id": "C0004482",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/",
        },
        {
            "drug": "mercaptopurine",
            "id": "C0000618",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/",
        },
        {
            "drug": "thioguanine",
            "id": "C0039902",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/",
        },
    ],
    "CYP2C9": [
        {
            "drug": "warfarin",
            "id": "C0043031",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-warfarin-and-cyp2c9-and-vkorc1/",
        }
    ],
    "VKORC1": [
        {
            "drug": "warfarin",
            "id": "C0043031",
            "recommendation": "https://cpicpgx.org/guidelines/guideline-for-warfarin-and-cyp2c9-and-vkorc1/",
        }
    ],
}

pgx_data = {
    "TPMTDiplotype": {
        "TTCCCCCC": ("*1/*1", "High activity"),
        "TTCCCCCG": ("*1/*2", "Intermediate activity"),
        "TCCCCTCC": (
            "*1/*3A",
            "Intermediate metabolizer, cannot rule out poor metabolizer, phenotypic testing is available to distinguish between these alleles",
        ),
        "TTCCCTCC": ("*1/*3B", "Intermediate activity"),
        "TCCCCCCC": ("*1/*3C", "Intermediate activity"),
        "TTCTCCCC": ("*1/*4", "Intermediate activity"),
        "TTCCCCGG": ("*2/*2", "Low activity"),
        "TCCCCTCG": ("*2/*3A", "Low activity"),
        "TTCCCTCG": ("*2/*3B", "Low activity"),
        "TCCCCCCG": ("*2/*3C", "Low activity"),
        "TTCTCCCG": ("*2/*4", "Low activity"),
        "CCCCTTCC": ("*3A/*3A", "Low activity"),
        "TCCCTTCC": ("*3A/*3B", "Low activity"),
        "CCCCCTCC": ("*3A/*3C", "Low activity"),
        "TCCTCTCC": ("*3A/*4", "Low activity"),
        "TTCCTTCC": ("*3B/*3B", "Low activity"),
        "TTCTCTCC": ("*3B/*4", "Low activity"),
        "CCCCCCCC": ("*3C/*3C", "Low activity"),
        "TCCTCCCC": ("*3C/*4", "Low activity"),
        "TTTTCCCC": ("*4/*4", "Low activity"),
    },
    "TPMTInterp": {
        "High activity": "This individual is homozygous for the normal high activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have normal TPMT function. Individuals with this diplotype are expected to have a normal response to mercaptopurine, azathioprine and thioguanine. A normal dose of thiopurine and adjustment following the disease-specific guidelines is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.",
        "Intermediate activity": "This individual is heterozygous for a low activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have intermediate TPMT activity. Individuals with this diplotype are at increased risk for toxic response to mercaptopurine, azathioprine and thioguanine. A reduced starting dose of thiopurine drugs is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.",
        "Intermediate metabolizer, cannot rule out poor metabolizer, phenotypic testing is available to distinguish between these alleles": "In this individual, the current test cannot distinguish between the *1/*3A and *3B/*3C diplotypes in the TPMT gene and additional phenotypic testing is recommended. Individuals with the *1/*3A diplotype (intermediate TPMT activity) have one non-functional allele and are at increased risk for thiopurine drug-related toxicity and myelosuppression. Thus, a reduced starting dose of thiopurine drugs (mercaptopurine, azathioprine, thioguanine) is recommended. However, this test cannot rule out the much rarer *3B/*3C diplotype (low or no TPMT activity) associated with two non-functional alleles and a very high and potentially fatal risk for thiopurine drug-related toxicity. In that case, a drastically reduced dose of thiopurine or alternative non-thiopurine immunosuppressant therapy is recommended. Given the equivocal result of this test, additional TPMT phenotyping is recommended to optimize therapy. This information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.",
        "Low activity": "This individual is homozygous for the low activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have low TPMT activity. Individuals with this diplotype are at increased risk for potentially fatal response to mercaptopurine, azathioprine and thioguanine. Reduced dose of thiopurine or alternative non-thiopurine immunosuppressant therapy is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.",
    },
    "IFNL3Interp": {
        "CC": (
            "Favorable response genotype",
            "This individual is homozygous for the rs12979860 C/C allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have an increased likelihood of response (higher sustained virologic response rate) to peginterferon alfa and ribavirin therapy as compared with patients with unfavorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.",
        ),
        "CT": (
            "Unfavorable response genotype",
            "This individual is heterozygous for the rs12979860 C/T allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have a reduced likelihood of response (lower sustained virological response rate) to peginterferon alfa and ribavirin therapy as compared with patients with favorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.",
        ),
        "TT": (
            "Unfavorable response genotype",
            "This individual is homozygous for the rs12979860 T/T allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have a reduced likelihood of response (lower sustained virological response rate) to peginterferon alfa and ribavirin therapy as compared with patients with favorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.",
        ),
    },
    "DPYDDiplotype": {
        "TTCCAA": (
            "*1/*1",
            'Normal DPD activity and "normal" risk for fluoropyrimidine toxicity',
        ),
        "TTCTAA": (
            "*1/*2A",
            "Decreased DPD activity (leukocyte DPD activity at 30-70&#37; that of the normal population) and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TTCCAC": (
            "*1/*13",
            "Decreased DPD activity (leukocyte DPD activity at 30-70&#37; that of the normal population) and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TACCAA": (
            "*1/rs67376798",
            "Decreased DPD activity (leukocyte DPD activity at 30-70&#37; that of the normal population) and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TTTTAA": (
            "*2A/*2A",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TTCTAC": (
            "*2A/*13",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TACTAA": (
            "*2A/rs67376798",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TTCCCC": (
            "*13/*13",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "TACCAC": (
            "*13/rs67376798",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
        "AACCAA": (
            "rs67376798/rs67376798",
            "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs",
        ),
    },
    "DPYDInterp": {
        'Normal DPD activity and "normal" risk for fluoropyrimidine toxicity': 'This individual is homozygous for the functional allele of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur).  Based on the genotype result, this patient is predicted to have a normal DPD activity phenotype.  Individuals with this diplotype are expected to have "normal" risk for fluoropyrimidine toxicity. Recommendations include the use of label recommended dosage and administration. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.',
        "Decreased DPD activity (leukocyte DPD activity at 30-70&#37; that of the normal population) and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs": "This individual is heterozygous for a non-functional allele of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur). Based on the genotype result, this patient is predicted to have a decreased DPD activity phenotype. Individuals with this diplotype are expected to have increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs. Recommendations include to start with at least a 50&#37; reduction in starting dose, followed by titration of dose based on toxicityb or pharmacokinetic test (if available). Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.",
        "Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs": "This individual is homozygous for non-functional alleles of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur). Based on the genotype result, this patient is predicted to have a complete DPD deficiency phenotype. Individuals with this diplotype are expected to have increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs. Recommendations include the use of an alternative drug. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.",
    },
    "SLCO1B1Interp": {
        "TT": (
            "Normal function, Normal simvastatin induced myopathy risk",
            "This individual is homozygous for the rs4149056 T/T allele in the SLCO1B1 gene. This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and other drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have normal SLCO1B1 function. This means that there is no reason to adjust the dose of most medications that are affected by SLCO1B1 (including simvastatin) on the basis of SLCO1B1 genetic status. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.",
        ),
        "TC": (
            "Intermediate function, Intermediate simvastatin induced  myopathy risk",
            "This individual is heterozygous for the rs4149056 T/C allele in the SLCO1B1 gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and other drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have intermediate SLCO1B1 function. This patient may be at risk for an adverse response to medications that are affected by SLCO1B1. To avoid an untoward drug response, dose adjustments may be necessary for medications affected by SLCO1B1. If simvastatin is prescribed to a patient with intermediate SLCO1B1 function, there is an increased risk for developing simvastatin-associated myopathy; such patients may need a lower starting dose of simvastatin or an alternate statin agent. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.",
        ),
        "CC": (
            "Low function, High simvastatin induced myopathy risk",
            "This individual is homozygous for the rs4149056 C/C allele in the SLCO1B1 gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have low SLCO1B1 function. This patient may be at a high risk for an adverse response to medications that are affected by SLCO1B1. To avoid an untoward drug response, dose adjustments or alternative therapeutic agents may be necessary for medications affected by SLCO1B1. If simvastatin is prescribed to a patient with low SLCO1B1 function, there is a high risk of developing simvastatin-associated myopathy; such patients may need an alternative statin agent, and creatine kinase levels may need to be monitored routinely. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.",
        ),
    },
    "CYP2C19Diplotype": {
        "CCAATTGGGGGGTTCC": ("*1/*1", "Normal metabolizer"),
        "CCAATTGGGGGATTCC": ("*1/*2", "Intermediate metabolizer"),
        "CCAATTGGGAGGTTCC": ("*1/*3", "Intermediate metabolizer"),
        "CCAGTTGGGGGGTTCC": ("*1/*4A", "Intermediate metabolizer"),
        "CTAGTTGGGGGGTTCC": (
            "CYP2C19*1/*4B or CYP2C19*4A/*17",
            "Intermediate metabolizer",
        ),
        "CCAATTGGGGGGTTCT": ("*1/*5", "Intermediate metabolizer"),
        "CCAATTGAGGGGTTCC": ("*1/*6", "Intermediate metabolizer"),
        "CCAATTGGGGGGTACC": ("*1/*7", "Intermediate metabolizer"),
        "CCAATCGGGGGGTTCC": ("*1/*8", "Intermediate metabolizer"),
        "CTAATTGGGGGGTTCC": ("*1/*17", "Rapid metabolizer"),
        "CCAATTGGGGAATTCC": ("*2/*2", "Poor metabolizer"),
        "CCAATTGGGAGATTCC": ("*2/*3", "Poor metabolizer"),
        "CCAGTTGGGGGATTCC": ("*2/*4A", "Poor metabolizer"),
        "CTAGTTGGGGGATTCC": ("*2/*4B", "Poor metabolizer"),
        "CCAATTGGGGGATTCT": ("*2/*5", "Poor metabolizer"),
        "CCAATTGAGGGATTCC": ("*2/*6", "Poor metabolizer"),
        "CCAATTGGGGGATACC": ("*2/*7", "Poor metabolizer"),
        "CCAATCGGGGGATTCC": ("*2/*8", "Poor metabolizer"),
        "CTAATTGGGGGATTCC": ("*2/*17", "Intermediate metabolizer"),
        "CCAATTGGAAGGTTCC": ("*3/*3", "Poor metabolizer"),
        "CCAGTTGGGAGGTTCC": ("*3/*4A", "Poor metabolizer"),
        "CTAGTTGGGAGGTTCC": ("*3/*4B", "Poor metabolizer"),
        "CCAATTGGGAGGTTCT": ("*3/*5", "Poor metabolizer"),
        "CCAATTGAGAGGTTCC": ("*3/*6", "Poor metabolizer"),
        "CCAATTGGGAGGTACC": ("*3/*7", "Poor metabolizer"),
        "CCAATCGGGAGGTTCC": ("*3/*8", "Poor metabolizer"),
        "CTAATTGGGAGGTTCC": ("*3/*17", "Intermediate metabolizer"),
        "CCGGTTGGGGGGTTCC": ("*4A/*4A", "Poor metabolizer"),
        "CTGGTTGGGGGGTTCC": ("*4A/*4B", "Poor metabolizer"),
        "CCAGTTGGGGGGTTCT": ("*4A/*5", "Poor metabolizer"),
        "CCAGTTGAGGGGTTCC": ("*4A/*6", "Poor metabolizer"),
        "CCAGTTGGGGGGTACC": ("*4A/*7", "Poor metabolizer"),
        "CCAGTCGGGGGGTTCC": ("*4A/*8", "Poor metabolizer"),
        # "CTAGTTGGGGGGTTCC" : ("*4A/*17", "Intermediate metabolizer"), # merged with *1/*4B due to phasing issues
        "TTGGTTGGGGGGTTCC": ("*4B/*4B", "Poor metabolizer"),
        "CTAGTTGGGGGGTTCT": ("*4B/*5", "Poor metabolizer"),
        "CTAGTTGAGGGGTTCC": ("*4B/*6", "Poor metabolizer"),
        "CTAGTTGGGGGGTACC": ("*4B/*7", "Poor metabolizer"),
        "CTAGTCGGGGGGTTCC": ("*4B/*8", "Poor metabolizer"),
        "TTAGTTGGGGGGTTCC": ("*4B/*17", "Intermediate metabolizer"),
        "CCAATTGGGGGGTTTT": ("*5/*5", "Poor metabolizer"),
        "CCAATTGAGGGGTTCT": ("*5/*6", "Poor metabolizer"),
        "CCAATTGGGGGGTACT": ("*5/*7", "Poor metabolizer"),
        "CCAATCGGGGGGTTCT": ("*5/*8", "Poor metabolizer"),
        "CTAATTGGGGGGTTCT": ("*5/*17", "Intermediate metabolizer"),
        "CCAATTAAGGGGTTCC": ("*6/*6", "Poor metabolizer"),
        "CCAATTGAGGGGTACC": ("*6/*7", "Poor metabolizer"),
        "CCAATCGAGGGGTTCC": ("*6/*8", "Poor metabolizer"),
        "CTAATTGAGGGGTTCC": ("*6/*17", "Intermediate metabolizer"),
        "CCAATTGGGGGGAACC": ("*7/*7", "Poor metabolizer"),
        "CCAATCGGGGGGTACC": ("*7/*8", "Poor metabolizer"),
        "CTAATTGGGGGGTACC": ("*7/*17", "Intermediate metabolizer"),
        "CCAACCGGGGGGTTCC": ("*8/*8", "Poor metabolizer"),
        "CTAATCGGGGGGTTCC": ("*8/*17", "Intermediate metabolizer"),
        "TTAATTGGGGGGTTCC": ("*17/*17", "Ultrarapid metabolizer"),
        "CCAATTGAGGAATTCC": ("*2/[*2+*6]", "Poor metabolizer"),
    },
    "CYP2C19Interp": {
        "Ultrarapid metabolizer": "This individual is homozygous for the increased function allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 ultrarapid metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram.  For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/.  For voriconazole, the probability of attainment of therapeutic voriconazole concentrations in individuals with this genotype is small with standard dosing. An alternative agent that is not dependent on CYP2C19 metabolism as primary therapy in lieu of voriconazole such as isavuconazole, liposomal amphotericin B, and posaconazole, is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, an alternative drug not predominantly metabolized by CYP2D19 is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, an alternative drug not predominantly metabolized by CYP2D19 is recommended. If a tricyclic is warranted, therapeutic drug monitoring to guide dose adjustment is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.",
        "Rapid metabolizer": "This individual is heterozygous for the increased function allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 rapid metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, for adult patients, the probability of attainment of therapeutic voriconazole concentrations in individuals with this genotype is modest with standard dosing. An alternative agent that is not dependent on CYP2C19 metabolism as primary therapy in lieu of voriconazole such as isavuconazole, liposomal amphotericin B, and posaconazole, is recommended. For pediatric rapid metabolizer patients, therapy should be initiated at recommended standard of care dosing, then therapeutic dosing monitoring should be used to titrate dose to therapeutic trough concentrations. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, an alternative drug not predominantly metabolized by CYP2D19 is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, an alternative drug not predominantly metabolized by CYP2D19 is recommended. If a tricyclic is warranted, therapeutic drug monitoring to guide dose adjustment is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.",
        "Normal metabolizer": "This individual is homozygous for the wild type allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 normal metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram.  For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, normal voriconazole metabolism is expected in individuals with this genotype. Initiate therapy with recommended standard of care dosing. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.",
        "Intermediate metabolizer": "This individual is heterozygous for the non-functional allele of the CYP2C19 gene. Based on the genotype result, this patient is predicted to have a CYP2C19 intermediate metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have reduced platelet inhibition, increased residual platelet aggregation and increased risk for adverse cardiovascular events in response to clopidogrel. Alternative antiplatelet therapy (if no contraindication) is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, higher dose-adjusted trough concentrations of voriconazole compared to normal metabolizers is expected in individuals with this genotype. Initiate therapy with recommended standard of care dosing. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.",
        "Poor metabolizer": "This individual is homozygous for a non-functional allele of the CYP2C19 gene. Based on the genotype result, this patient is predicted to have a CYP2C19 poor metabolizer phenotype.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have significantly reduced platelet inhibition, increased residual platelet aggregation and increased risk for adverse cardiovascular events in response to clopidogrel. Alternative antiplatelet therapy (if no contraindication) is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, higher dose-adjusted trough concentrations of voriconazole are expected in individuals with this genotype and may increase the probability of adverse events. An alternative agent that is not dependent on CYP2C19 metabolism such as isavuconazole, liposomal amphotericin B, or posaconazole is recommended as primary therapy in lieu of voriconazole. A lower than standard dosage of voriconazole with careful therapeutic drug monitoring is another alternative. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, a 50&#37; reduction in starting dose is recommended with therapeutic drug monitoring to guide dose adjustment or select an alternate drug not predominantly metabolized by CYP2C19. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, a 50&#37; reduction in starting dose is recommended with therapeutic drug monitoring to guide dose adjustment. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.",
    },
    "CYP2C9Interp": {
        "Extensive metabolizer": "This individual is homozygous for the normal allele for the CYP2C9 gene. Based on the genotype result, this patient is predicted to have normal CYP2C9 function.",
        "Intermediate metabolizer": "This individual is heterozygous for the low function allele in the CYP2C9 gene. Based on the genotype result, this patient is predicted to have intermediate CYP2C9 function.",
        "Poor metabolizer": "This individual is homozygous for the low function allele in the CYP2C9 gene. Based on the genotype result, this patient is predicted to have poor CYP2C9 function.",
    },
    "CYP2C9Diplotype": {
        "CCAA": ("*1/*1", "Extensive metabolizer"),
        "CTAA": ("*1/*2", "Intermediate metabolizer"),
        "CCAC": ("*1/*3", "Intermediate metabolizer"),
        "TTAA": ("*2/*2", "Poor metabolizer"),
        "CTAC": ("*2/*3", "Poor metabolizer"),
        "CCCC": ("*3/*3", "Poor metabolizer"),
    },
    "VKORC1Interp": {
        "CC": (
            "Low sensitivity to Warfarin",
            "This individual is also homozygous for the normal allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have normal sensitivity to warfarin.",
        ),
        "CT": (
            "Medium sensitivity to Warfarin",
            "This individual is also heterozygous for the variant allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have medium sensitivity to warfarin.",
        ),
        "TT": (
            "High sensitivity to Warfarin",
            "This individual is also homozygous for the variant allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have high sensitivity to warfarin.",
        ),
    },
}

pickle.dump(pgx_data, open("pgx_data.p", "w"))
pickle.dump(pgx_drug_recommendations, open("pgx_drug_recommendations.p", "w"))
