import json


def write_json(filename, reportData):
    with open(filename, "w") as outfile:
        json.dump(reportData, outfile)
    print(filename)


def format_dbutil_date(date):
    year, month, day = date.split("/")
    return "%s/%s/%s" % (month, day, year)


def get_addenda_tag(addenda, corrections):
    addenda_tag = ""
    if len(corrections) > 0:
        date = corrections[-1]["date"]  # get the date of the latest
        addenda_tag = ".corrected%s" % (date.replace("/", "-"))
    elif len(addenda) > 0:
        date = addenda[-1]["date"]
        addenda_tag = ".addended%s" % (date.replace("/", "-"))
    return addenda_tag


def clean_text(text):
    return text.replace("%2C", ",").replace("%3B", ";").replace("%25", "%")


def get_inheritance_pattern(inheritance, config):
    if inheritance == "AD/AR, AR- Ataxia/AD-cancer":
        inheritance = "AD/AR"
    return config.get("inheritance_patterns", inheritance)


def build_comment(is_positive, is_preliminary, config):
    comment = list()
    if is_positive:
        comment.append(config.get("positive_comment"))
        if is_preliminary:
            comment.append(config.get("preliminary_comment"))
    else:
        comment.append(config.get("negative_comment"))
    return comment
