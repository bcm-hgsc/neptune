import subprocess
import os
import logging
import glob


def get_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    return logger


def run_cmd(cmd, returnOutput=True):
    if type(cmd) is list:
        shell = False
        executable = None
        print((subprocess.list2cmdline(cmd)))
    else:
        shell = True
        executable = "/bin/bash"
        print(cmd)
    try:
        p = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=shell,
            executable=executable,
        )
        out, err = p.communicate()
        print((out, err))
        if p.returncode != 0:
            raise SystemError(err)
        if returnOutput:
            return out
    except Exception as e:
        print(e)


def makedir(folder):
    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
        except OSError:
            pass


def parse_gene_list(config, siteGenes):
    return set([g.strip() for g in config.get("genes", siteGenes).split(",")])


def get_key(chrom, pos, ref="", alt=""):
    return "%s|%s|%s|%s" % (chrom, pos, ref, alt)


def parse_vcf_info(info):
    vcfInfo = dict()
    for s in info.split(";"):
        r = s.split("=")
        key = r[0]
        value = "=".join(r[1:])
        vcfInfo[key.lower()] = value
    return vcfInfo


def get_classification(info):
    infoFields = parse_vcf_info(info)
    if "emerge_category" in infoFields:
        return infoFields["emerge_category"].upper()
    elif "classificationbcm" in infoFields:
        return infoFields["classificationbcm"].upper()


def find_path(path, zero_ok=False):
    paths = glob.glob(path)
    if len(paths) == 0:
        if zero_ok:
            return None
        raise PathExpandException("No path matching pattern: " + path)
    elif len(paths) > 1:
        raise PathExpandException(
            "More than 1 path matching pattern: {0}; please remove outdated files.\n{1}".format(
                path, paths
            )
        )
    return paths[0]


def get_sample_definition(config, fcid, sampleid):
    return find_path(
        config.get("sample_definition_path").format(
            flowcell=fcid, sample=sampleid
        )
    )


class PathExpandException(Exception):
    pass
