import sys
import sqlite3
import datetime


class DBUtil:
    def __init__(self, config):
        self.config = config

    def _connect(self):
        return self.config.get("flowcell_status_db")

    def get_corrections(self, internal_id):
        db = self._connect()
        conn = sqlite3.connect(db)
        c = conn.cursor()
        sql = "SELECT created, text, textDetail FROM corrected C WHERE C.sampleName = ? ORDER BY created ASC;"
        c.execute(sql, (internal_id,))
        rows = c.fetchall()
        corrections = list()
        for row in rows:
            created, text, text_detail = row
            dt = datetime.datetime.strptime(created, "%Y-%m-%d %H:%M:%S")
            corrections.append(
                {
                    "date": "%s/%s/%s"
                    % (dt.year, dt.strftime("%m"), dt.strftime("%d")),
                    "text": text,
                    "text_detail": text_detail,
                }
            )
        conn.close()
        return corrections

    def get_addenda(self, internal_id):
        db = self._connect()
        conn = sqlite3.connect(db)
        c = conn.cursor()
        sql = "SELECT created, text, textDetail FROM reportAddendum RA WHERE RA.sampleName = ? ORDER BY created ASC;"
        c.execute(sql, (internal_id,))
        rows = c.fetchall()
        addenda = list()
        for row in rows:
            created, text, text_detail = row
            dt = datetime.datetime.strptime(created, "%Y-%m-%d %H:%M:%S")
            addenda.append(
                {
                    "date": "%s/%s/%s"
                    % (dt.year, dt.strftime("%m"), dt.strftime("%d")),
                    "text": text,
                    "text_detail": text_detail,
                }
            )
        conn.close()
        return addenda

    def add_correction(self, fcid, internal_id, reason, reason_detail):
        db = self._connect()
        conn = sqlite3.connect(db)
        try:
            if reason_detail is None:
                reason_detail = reason
            c = conn.cursor()
            sql = (
                "insert into corrected (sampleID, sampleName, text, textDetail) "
                "select S.id, S.sampleName, ?, ? "
                "from samples S "
                "inner join Flowcells F "
                "where sampleName = ? and F.flowcellName like ?;"
            )
            c.execute(sql, (reason, reason_detail, internal_id, '%' + fcid))
            print("Updated {0} rows".format(c.rowcount))
            conn.commit()
        except Exception as e:
            raise e
        finally:
            pass

    def add_on_hold(self, batch_id, internal_id, reason):
        db = self._connect()
        conn = sqlite3.connect(db)
        try:
            c = conn.cursor()
            sql = "UPDATE samples SET onHold = 1, onHoldReason = ? WHERE sampleName = ? AND flowcellID = (SELECT id from Flowcells F where F.batchID = ?)"
            c.execute(sql, (reason, internal_id, batch_id))
            print(("updated", c.rowcount, "rows"))
            conn.commit()
        except Exception as e:
            raise e
        finally:
            pass

    def add_sanger_confirmed(
        self, batchID, sampleName, chrom, pos, ref, alt, sangerRequired
    ):
        db = self._connect()
        conn = sqlite3.connect(db)
        confirmedBySanger = "1"
        if sangerRequired == "F":
            confirmedBySanger = "0"
            sangerRequired = "0"
        else:
            sangerRequired = "1"
        try:
            pass
            c = conn.cursor()
            sql = (
                "INSERT INTO variant (chrom, pos, ref, alt, confirmedBySanger, flowcellID, sampleID, sangerRequired) "
                + "SELECT ?, ?, ?, ?, ?, F.id, S.id, ?"
                + "FROM Flowcells F INNER JOIN Samples s on S.flowcellID = F.id "
                + "WHERE F.batchID = ? AND S.sampleName = ?"
            )
            print(sql)
            c.execute(
                sql,
                (
                    chrom,
                    pos,
                    ref,
                    alt,
                    confirmedBySanger,
                    sangerRequired,
                    batchID,
                    sampleName,
                ),
            )
            print(("updated", c.rowcount, "rows"))
            if c.rowcount == 1:
                conn.commit()
            else:
                print("Updated unexpected number of rows, will not commit.")
            conn.close()
        except Exception as e:
            raise e
        finally:
            pass

    def get_on_hold_samples(self, flowcell_id):
        db = self._connect()
        conn = sqlite3.connect(db)
        result = dict()
        c = conn.cursor()
        sql = "SELECT S.sampleName, S.onHoldReason, F.flowcellName FROM samples S INNER JOIN flowcells F WHERE S.onHold = 1 and S.flowcellID = F.id and F.flowcellName LIKE ?"
        c.execute(sql, ("%%%s" % flowcell_id,))
        rows = c.fetchall()
        fc = set()
        for row in rows:
            sampleName, reason, flowcellName = row
            result[sampleName] = reason
            fc.add(flowcellName)
        conn.close()
        assert (
            len(fc) <= 1
        ), "There should only be one flowcell's worth of samples in the results"
        return result

    def get_confirmed_cnvs(self, flowcell_id):
        print("Loading Confirmed CNVs")
        db = self._connect()
        conn = sqlite3.connect(db)
        result = dict()  # key is sampleName
        c = conn.cursor()
        sql = "SELECT C.sampleName, C.batchID, C.geneExon, C.transcript, C.confirmedExons, C.medicalRelevance, C.variantInterpretation FROM confirmedCNV C INNER JOIN flowcells F WHERE F.batchID = C.batchID and F.flowcellName LIKE ?"
        c.execute(sql, ("%%%s" % flowcell_id,))
        print(("\tSQL:", sql, ("%%%s" % flowcell_id)))
        rows = c.fetchall()
        fc = set()
        for row in rows:
            (
                sampleName,
                batchID,
                geneExon,
                transcript,
                confirmedExons,
                medicalRelevance,
                variantInterpretation,
            ) = row
            print("\tCONFIRMED: ", row)
            if sampleName not in result:
                result[sampleName] = dict()
            result[sampleName][geneExon] = {
                "transcript": transcript,
                "confirmed_exons": confirmedExons,
                "medical_relevance": medicalRelevance,
                "variant_interpretation": variantInterpretation,
            }
            fc.add(batchID)
        conn.close()
        assert (
            len(fc) <= 1
        ), "There should only be one flowcell's worth of samples in the results"
        return result

    def get_confirmed_variants(self, sample):
        db = self._connect()
        conn = sqlite3.connect(db)
        result = dict()
        c = conn.cursor()
        sql = (
            "SELECT chrom, pos, ref, alt, sangerRequired FROM variant V "
            + "INNER JOIN samples S on V.sampleID = S.id where S.sampleName = ? and (V.confirmedBySanger = 1 or V.sangerRequired=0);"
        )
        c.execute(sql, (sample.internal_id,))
        print(("Getting confirmed variants for: ", sample.internal_id))
        rows = c.fetchall()
        print(sql)
        for row in rows:
            chrom, pos, ref, alt, sanger_required = row
            key = "%s|%s" % (chrom, pos)  # TODO unify this logic elsewhere
            print(("Sanger required", key, sanger_required))
            # All of these variants are "confirmed", but some of them did not actually require sanger confirmation
            # The value in the hash here indicates whether or not sanger was required
            result[key] = sanger_required == 1
        conn.close()
        return result
