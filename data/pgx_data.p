(dp0
S'TPMTDiplotype'
p1
(dp2
S'CCCCCCCC'
p3
(S'*3C/*3C'
p4
S'Low activity'
p5
tp6
sS'TCCTCTCC'
p7
(S'*3A/*4'
p8
g5
tp9
sS'TCCCTTCC'
p10
(S'*3A/*3B'
p11
g5
tp12
sS'TTCTCCCC'
p13
(S'*1/*4'
p14
S'Intermediate activity'
p15
tp16
sS'TTCCCCCG'
p17
(S'*1/*2'
p18
g15
tp19
sS'TTTTCCCC'
p20
(S'*4/*4'
p21
g5
tp22
sS'TTCCCTCC'
p23
(S'*1/*3B'
p24
g15
tp25
sS'TTCCCCCC'
p26
(S'*1/*1'
p27
S'High activity'
p28
tp29
sS'TTCTCCCG'
p30
(S'*2/*4'
p31
g5
tp32
sS'TTCTCTCC'
p33
(S'*3B/*4'
p34
g5
tp35
sS'CCCCCTCC'
p36
(S'*3A/*3C'
p37
g5
tp38
sS'TTCCTTCC'
p39
(S'*3B/*3B'
p40
g5
tp41
sS'CCCCTTCC'
p42
(S'*3A/*3A'
p43
g5
tp44
sS'TTCCCCGG'
p45
(S'*2/*2'
p46
g5
tp47
sS'TTCCCTCG'
p48
(S'*2/*3B'
p49
g5
tp50
sS'TCCCCCCG'
p51
(S'*2/*3C'
p52
g5
tp53
sS'TCCCCTCG'
p54
(S'*2/*3A'
p55
g5
tp56
sS'TCCCCCCC'
p57
(S'*1/*3C'
p58
g15
tp59
sS'TCCCCTCC'
p60
(S'*1/*3A'
p61
S'Intermediate metabolizer, cannot rule out poor metabolizer, phenotypic testing is available to distinguish between these alleles'
p62
tp63
sS'TCCTCCCC'
p64
(S'*3C/*4'
p65
g5
tp66
ssS'CYP2C19Interp'
p67
(dp68
S'Ultrarapid metabolizer'
p69
S'This individual is homozygous for the increased function allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 ultrarapid metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram.  For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/.  For voriconazole, the probability of attainment of therapeutic voriconazole concentrations in individuals with this genotype is small with standard dosing. An alternative agent that is not dependent on CYP2C19 metabolism as primary therapy in lieu of voriconazole such as isavuconazole, liposomal amphotericin B, and posaconazole, is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, an alternative drug not predominantly metabolized by CYP2D19 is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, an alternative drug not predominantly metabolized by CYP2D19 is recommended. If a tricyclic is warranted, therapeutic drug monitoring to guide dose adjustment is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.'
p70
sS'Rapid metabolizer'
p71
S'This individual is heterozygous for the increased function allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 rapid metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, for adult patients, the probability of attainment of therapeutic voriconazole concentrations in individuals with this genotype is modest with standard dosing. An alternative agent that is not dependent on CYP2C19 metabolism as primary therapy in lieu of voriconazole such as isavuconazole, liposomal amphotericin B, and posaconazole, is recommended. For pediatric rapid metabolizer patients, therapy should be initiated at recommended standard of care dosing, then therapeutic dosing monitoring should be used to titrate dose to therapeutic trough concentrations. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, an alternative drug not predominantly metabolized by CYP2D19 is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, an alternative drug not predominantly metabolized by CYP2D19 is recommended. If a tricyclic is warranted, therapeutic drug monitoring to guide dose adjustment is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.'
p72
sS'Intermediate metabolizer'
p73
S'This individual is heterozygous for the non-functional allele of the CYP2C19 gene. Based on the genotype result, this patient is predicted to have a CYP2C19 intermediate metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have reduced platelet inhibition, increased residual platelet aggregation and increased risk for adverse cardiovascular events in response to clopidogrel. Alternative antiplatelet therapy (if no contraindication) is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, higher dose-adjusted trough concentrations of voriconazole compared to normal metabolizers is expected in individuals with this genotype. Initiate therapy with recommended standard of care dosing. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.'
p74
sS'Poor metabolizer'
p75
S'This individual is homozygous for a non-functional allele of the CYP2C19 gene. Based on the genotype result, this patient is predicted to have a CYP2C19 poor metabolizer phenotype.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram. For clopidogrel, individuals with this diplotype are expected to have significantly reduced platelet inhibition, increased residual platelet aggregation and increased risk for adverse cardiovascular events in response to clopidogrel. Alternative antiplatelet therapy (if no contraindication) is recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, higher dose-adjusted trough concentrations of voriconazole are expected in individuals with this genotype and may increase the probability of adverse events. An alternative agent that is not dependent on CYP2C19 metabolism such as isavuconazole, liposomal amphotericin B, or posaconazole is recommended as primary therapy in lieu of voriconazole. A lower than standard dosage of voriconazole with careful therapeutic drug monitoring is another alternative. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, a 50&#37; reduction in starting dose is recommended with therapeutic drug monitoring to guide dose adjustment or select an alternate drug not predominantly metabolized by CYP2C19. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, a 50&#37; reduction in starting dose is recommended with therapeutic drug monitoring to guide dose adjustment. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.'
p76
sS'Normal metabolizer'
p77
S'This individual is homozygous for the wild type allele of the CYP2C19 gene.  Based on the genotype result, this patient is predicted to have a CYP2C19 normal metabolizer phenotype. This genotype information can be used by patients and clinicians as part of the shared decision-making process for several drugs metabolized by CYP2C19 including clopidogrel, voriconazole, amitriptyline, citalopram and escitalopram.  For clopidogrel, individuals with this diplotype are expected to have normal platelet inhibition and normal residual platelet aggregation in response to clopidogrel. Label recommended dosage and administration are recommended. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/. For voriconazole, normal voriconazole metabolism is expected in individuals with this genotype. Initiate therapy with recommended standard of care dosing. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/. For citalopram and escitalopram, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/. For amitriptyline, initiate therapy with recommended starting dose. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/. For citalopram, escitalopram and amitriptyline, if CYP2D6 genotyping is available, refer to the current guidelines for dosing recommendations.'
p78
ssS'IFNL3Interp'
p79
(dp80
S'CC'
p81
(S'Favorable response genotype'
p82
S'This individual is homozygous for the rs12979860 C/C allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have an increased likelihood of response (higher sustained virologic response rate) to peginterferon alfa and ribavirin therapy as compared with patients with unfavorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.'
p83
tp84
sS'TT'
p85
(S'Unfavorable response genotype'
p86
S'This individual is homozygous for the rs12979860 T/T allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have a reduced likelihood of response (lower sustained virological response rate) to peginterferon alfa and ribavirin therapy as compared with patients with favorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.'
p87
tp88
sS'CT'
p89
(g86
S'This individual is heterozygous for the rs12979860 C/T allele in the IFNL3 gene. This variant is the strongest baseline predictor of response to peginterferon alfa and ribavirin therapy in previously untreated patients and can be used by patients and clinicians as part of the shared decision-making process for initiating treatment for hepatitis C virus infection. Based on the genotype result, this patient is predicted to have a reduced likelihood of response (lower sustained virological response rate) to peginterferon alfa and ribavirin therapy as compared with patients with favorable response genotype. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/.'
p90
tp91
ssS'CYP2C9Interp'
p92
(dp93
S'Extensive metabolizer'
p94
S'This individual is homozygous for the normal allele for the CYP2C9 gene. Based on the genotype result, this patient is predicted to have normal CYP2C9 function.'
p95
sg73
S'This individual is heterozygous for the low function allele in the CYP2C9 gene. Based on the genotype result, this patient is predicted to have intermediate CYP2C9 function.'
p96
sg75
S'This individual is homozygous for the low function allele in the CYP2C9 gene. Based on the genotype result, this patient is predicted to have poor CYP2C9 function.'
p97
ssS'VKORC1Interp'
p98
(dp99
g81
(S'Low sensitivity to Warfarin'
p100
S'This individual is also homozygous for the normal allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have normal sensitivity to warfarin.'
p101
tp102
sg85
(S'High sensitivity to Warfarin'
p103
S'This individual is also homozygous for the variant allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have high sensitivity to warfarin.'
p104
tp105
sg89
(S'Medium sensitivity to Warfarin'
p106
S'This individual is also heterozygous for the variant allele for the VKORC1 gene. Expression level of the VKORC1 gene is associated with warfarin sensitivity. Based on the genotype result, this patient is predicted to have medium sensitivity to warfarin.'
p107
tp108
ssS'DPYDDiplotype'
p109
(dp110
S'TTCCAC'
p111
(S'*1/*13'
p112
S'Decreased DPD activity (leukocyte DPD activity at 30-70&#37; that of the normal population) and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs'
p113
tp114
sS'TACCAC'
p115
(S'*13/rs67376798'
p116
S'Complete DPD deficiency and increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs'
p117
tp118
sS'TTCCAA'
p119
(g27
S'Normal DPD activity and "normal" risk for fluoropyrimidine toxicity'
p120
tp121
sS'TACCAA'
p122
(S'*1/rs67376798'
p123
g113
tp124
sS'TTTTAA'
p125
(S'*2A/*2A'
p126
g117
tp127
sS'AACCAA'
p128
(S'rs67376798/rs67376798'
p129
g117
tp130
sS'TTCCCC'
p131
(S'*13/*13'
p132
g117
tp133
sS'TACTAA'
p134
(S'*2A/rs67376798'
p135
g117
tp136
sS'TTCTAA'
p137
(S'*1/*2A'
p138
g113
tp139
sS'TTCTAC'
p140
(S'*2A/*13'
p141
g117
tp142
ssS'SLCO1B1Interp'
p143
(dp144
g81
(S'Low function, High simvastatin induced myopathy risk'
p145
S'This individual is homozygous for the rs4149056 C/C allele in the SLCO1B1 gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have low SLCO1B1 function. This patient may be at a high risk for an adverse response to medications that are affected by SLCO1B1. To avoid an untoward drug response, dose adjustments or alternative therapeutic agents may be necessary for medications affected by SLCO1B1. If simvastatin is prescribed to a patient with low SLCO1B1 function, there is a high risk of developing simvastatin-associated myopathy; such patients may need an alternative statin agent, and creatine kinase levels may need to be monitored routinely. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.'
p146
tp147
sg85
(S'Normal function, Normal simvastatin induced myopathy risk'
p148
S'This individual is homozygous for the rs4149056 T/T allele in the SLCO1B1 gene. This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and other drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have normal SLCO1B1 function. This means that there is no reason to adjust the dose of most medications that are affected by SLCO1B1 (including simvastatin) on the basis of SLCO1B1 genetic status. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.'
p149
tp150
sS'TC'
p151
(S'Intermediate function, Intermediate simvastatin induced  myopathy risk'
p152
S'This individual is heterozygous for the rs4149056 T/C allele in the SLCO1B1 gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for simvastatin and other drugs affected by SLCO1B1. Based on the genotype result, this patient is predicted to have intermediate SLCO1B1 function. This patient may be at risk for an adverse response to medications that are affected by SLCO1B1. To avoid an untoward drug response, dose adjustments may be necessary for medications affected by SLCO1B1. If simvastatin is prescribed to a patient with intermediate SLCO1B1 function, there is an increased risk for developing simvastatin-associated myopathy; such patients may need a lower starting dose of simvastatin or an alternate statin agent. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/.'
p153
tp154
ssS'DPYDInterp'
p155
(dp156
g117
S'This individual is homozygous for non-functional alleles of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur). Based on the genotype result, this patient is predicted to have a complete DPD deficiency phenotype. Individuals with this diplotype are expected to have increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs. Recommendations include the use of an alternative drug. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.'
p157
sg120
S'This individual is homozygous for the functional allele of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur).  Based on the genotype result, this patient is predicted to have a normal DPD activity phenotype.  Individuals with this diplotype are expected to have "normal" risk for fluoropyrimidine toxicity. Recommendations include the use of label recommended dosage and administration. Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.'
p158
sg113
S'This individual is heterozygous for a non-functional allele of the DPYD gene.  This genotype information can be used by patients and clinicians as part of the shared decision-making process for fluoropyrimidines (capecitabine, fluorouracil, tegafur). Based on the genotype result, this patient is predicted to have a decreased DPD activity phenotype. Individuals with this diplotype are expected to have increased risk for severe or even fatal drug toxicity when treated with fluoropyrimidine drugs. Recommendations include to start with at least a 50&#37; reduction in starting dose, followed by titration of dose based on toxicityb or pharmacokinetic test (if available). Refer to current guidelines for dosage and recommendations at https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/.'
p159
ssS'CYP2C19Diplotype'
p160
(dp161
S'CCAATTGGGAGATTCC'
p162
(S'*2/*3'
p163
g75
tp164
sS'CCAATTGAGGGGTTCT'
p165
(S'*5/*6'
p166
g75
tp167
sS'CCAATTGGGGGGTTCT'
p168
(S'*1/*5'
p169
g73
tp170
sS'CCAGTTGGGAGGTTCC'
p171
(S'*3/*4A'
p172
g75
tp173
sS'CTAATTGAGGGGTTCC'
p174
(S'*6/*17'
p175
g73
tp176
sS'TTGGTTGGGGGGTTCC'
p177
(S'*4B/*4B'
p178
g75
tp179
sS'CCAATCGGGGGGTTCC'
p180
(S'*1/*8'
p181
g73
tp182
sS'CCAATTGGGGGGTTCC'
p183
(g27
g77
tp184
sS'CCAATCGGGGGGTACC'
p185
(S'*7/*8'
p186
g75
tp187
sS'CCAATTGAGGGGTTCC'
p188
(S'*1/*6'
p189
g73
tp190
sS'CCAATCGGGGGGTTCT'
p191
(S'*5/*8'
p192
g75
tp193
sS'TTAATTGGGGGGTTCC'
p194
(S'*17/*17'
p195
g69
tp196
sS'CCGGTTGGGGGGTTCC'
p197
(S'*4A/*4A'
p198
g75
tp199
sS'CCAATTGGGGGGTACT'
p200
(S'*5/*7'
p201
g75
tp202
sS'CTAATTGGGAGGTTCC'
p203
(S'*3/*17'
p204
g73
tp205
sS'CCAATTGGGGGGTACC'
p206
(S'*1/*7'
p207
g73
tp208
sS'CCAGTTGGGGGGTACC'
p209
(S'*4A/*7'
p210
g75
tp211
sS'CCAATTGGGAGGTTCC'
p212
(S'*1/*3'
p213
g73
tp214
sS'CTAATTGGGGGGTTCT'
p215
(S'*5/*17'
p216
g73
tp217
sS'CTAGTCGGGGGGTTCC'
p218
(S'*4B/*8'
p219
g75
tp220
sS'CCAATCGGGGGATTCC'
p221
(S'*2/*8'
p222
g75
tp223
sS'CTAATTGGGGGGTTCC'
p224
(S'*1/*17'
p225
g71
tp226
sS'CTAATCGGGGGGTTCC'
p227
(S'*8/*17'
p228
g73
tp229
sS'CCAATTGGGAGGTTCT'
p230
(S'*3/*5'
p231
g75
tp232
sS'CTGGTTGGGGGGTTCC'
p233
(S'*4A/*4B'
p234
g75
tp235
sS'CCAATTGGGGAATTCC'
p236
(g46
g75
tp237
sS'CTAATTGGGGGGTACC'
p238
(S'*7/*17'
p239
g73
tp240
sS'CCAATTGGGGGATACC'
p241
(S'*2/*7'
p242
g75
tp243
sS'CCAATTAAGGGGTTCC'
p244
(S'*6/*6'
p245
g75
tp246
sS'CCAATTGAGGGATTCC'
p247
(S'*2/*6'
p248
g75
tp249
sS'CCAACCGGGGGGTTCC'
p250
(S'*8/*8'
p251
g75
tp252
sS'CCAATTGAGAGGTTCC'
p253
(S'*3/*6'
p254
g75
tp255
sS'TTAGTTGGGGGGTTCC'
p256
(S'*4B/*17'
p257
g73
tp258
sS'CCAATTGAGGAATTCC'
p259
(S'*2/[*2+*6]'
p260
g75
tp261
sS'CCAATTGGGGGATTCC'
p262
(g18
g73
tp263
sS'CTAATTGGGGGATTCC'
p264
(S'*2/*17'
p265
g73
tp266
sS'CCAATTGGAAGGTTCC'
p267
(S'*3/*3'
p268
g75
tp269
sS'CCAGTTGAGGGGTTCC'
p270
(S'*4A/*6'
p271
g75
tp272
sS'CCAATTGGGGGATTCT'
p273
(S'*2/*5'
p274
g75
tp275
sS'CTAGTTGGGGGGTACC'
p276
(S'*4B/*7'
p277
g75
tp278
sS'CCAGTTGGGGGGTTCT'
p279
(S'*4A/*5'
p280
g75
tp281
sS'CCAATTGGGGGGAACC'
p282
(S'*7/*7'
p283
g75
tp284
sS'CTAGTTGGGGGATTCC'
p285
(S'*2/*4B'
p286
g75
tp287
sS'CTAGTTGGGGGGTTCT'
p288
(S'*4B/*5'
p289
g75
tp290
sS'CCAGTTGGGGGATTCC'
p291
(S'*2/*4A'
p292
g75
tp293
sS'CCAGTTGGGGGGTTCC'
p294
(S'*1/*4A'
p295
g73
tp296
sS'CTAGTTGGGGGGTTCC'
p297
(S'CYP2C19*1/*4B or CYP2C19*4A/*17'
p298
g73
tp299
sS'CTAGTTGAGGGGTTCC'
p300
(S'*4B/*6'
p301
g75
tp302
sS'CCAATCGAGGGGTTCC'
p303
(S'*6/*8'
p304
g75
tp305
sS'CCAATTGAGGGGTACC'
p306
(S'*6/*7'
p307
g75
tp308
sS'CCAGTCGGGGGGTTCC'
p309
(S'*4A/*8'
p310
g75
tp311
sS'CCAATTGGGGGGTTTT'
p312
(S'*5/*5'
p313
g75
tp314
sS'CCAATTGGGAGGTACC'
p315
(S'*3/*7'
p316
g75
tp317
sS'CCAATCGGGAGGTTCC'
p318
(S'*3/*8'
p319
g75
tp320
sS'CTAGTTGGGAGGTTCC'
p321
(S'*3/*4B'
p322
g75
tp323
ssS'CYP2C9Diplotype'
p324
(dp325
S'CTAC'
p326
(g163
g75
tp327
sS'CTAA'
p328
(g18
g73
tp329
sS'TTAA'
p330
(g46
g75
tp331
sS'CCCC'
p332
(g268
g75
tp333
sS'CCAA'
p334
(g27
g94
tp335
sS'CCAC'
p336
(g213
g73
tp337
ssS'PGXrecommendations'
p338
(dp339
S'IFNL3'
p340
S'https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/'
p341
sS'DPYD'
p342
S'https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/'
p343
sS'TPMT'
p344
S'https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/'
p345
sS'CYP2C9/VKORC1'
p346
S'https://cpicpgx.org/guidelines/guideline-for-warfarin-and-cyp2c9-and-vkorc1/'
p347
sS'SLCO1B1'
p348
S'https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/'
p349
sS'CYP2C19'
p350
(lp351
S'https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/'
p352
aS'https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/'
p353
aS'https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/'
p354
aS'https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/'
p355
assS'TPMTInterp'
p356
(dp357
g62
S'In this individual, the current test cannot distinguish between the *1/*3A and *3B/*3C diplotypes in the TPMT gene and additional phenotypic testing is recommended. Individuals with the *1/*3A diplotype (intermediate TPMT activity) have one non-functional allele and are at increased risk for thiopurine drug-related toxicity and myelosuppression. Thus, a reduced starting dose of thiopurine drugs (mercaptopurine, azathioprine, thioguanine) is recommended. However, this test cannot rule out the much rarer *3B/*3C diplotype (low or no TPMT activity) associated with two non-functional alleles and a very high and potentially fatal risk for thiopurine drug-related toxicity. In that case, a drastically reduced dose of thiopurine or alternative non-thiopurine immunosuppressant therapy is recommended. Given the equivocal result of this test, additional TPMT phenotyping is recommended to optimize therapy. This information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.'
p358
sg5
S'This individual is homozygous for the low activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have low TPMT activity. Individuals with this diplotype are at increased risk for potentially fatal response to mercaptopurine, azathioprine and thioguanine. Reduced dose of thiopurine or alternative non-thiopurine immunosuppressant therapy is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.'
p359
sg28
S'This individual is homozygous for the normal high activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have normal TPMT function. Individuals with this diplotype are expected to have a normal response to mercaptopurine, azathioprine and thioguanine. A normal dose of thiopurine and adjustment following the disease-specific guidelines is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.'
p360
sg15
S'This individual is heterozygous for a low activity allele of the TPMT gene.  Decreased TPMT gene activity is associated with toxicity and myelosuppression in response to thiopurines, and this genotype information can be used by patients and clinicians as part of the shared decision-making process for initiating treatment.  Based on the genotype result, this patient is predicted to have intermediate TPMT activity. Individuals with this diplotype are at increased risk for toxic response to mercaptopurine, azathioprine and thioguanine. A reduced starting dose of thiopurine drugs is recommended. Refer to current guidelines for dosage and recommendations for each specific thiopurine drug at https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/.'
p361
ss.