import re
import copy
import argparse
from config.config import PROJECT_CONFIGS, OUTPUT_TYPES
from utils.db_util import DBUtil
from components.default_project.data_builder import DataBuilder
from utils.neptune_util import get_key, parse_vcf_info, get_sample_definition


class RenderPreReport:
    """
    Loads data from external sources, creates structured output files
    """

    def __init__(self, args, config, module_loader):
        self.args = self._setup_args(args)
        config.load_project_config(self.args.project)
        config.load_output_types_config(self.args.output_types)
        self.config = config
        self.module_loader = module_loader
        self.dbUtil = DBUtil(self.config)
        self.output_formatters = list()
        for output in self.args.output_types:
            output_formatters = self.module_loader.load_module(
                self.config.get(output + "_module")
            )
            self.output_formatters.append(output_formatters(config, self.dbUtil))
        self.data_builder = DataBuilder(self.config, self.dbUtil, self.args.deidentify)

    def _setup_args(self, args):
        parser = argparse.ArgumentParser(
            prog='render-pre-report',
            description=self.__doc__.strip()
        )
        parser.add_argument(
            "-p",
            "--project",
            metavar="PROJECT",
            help="project type",
            choices=list(PROJECT_CONFIGS.keys()),
            required=True,
        )
        parser.add_argument(
            "-f",
            "--fcid",
            help="Flowcell ID to run",
            required=True,
        )
        parser.add_argument(
            "-i",
            "--sample-ids",
            nargs="+",
            metavar='SAMPLE_ID',
            help="One or more sample IDs in the sample flowcell path",
            required=True,
        )
        parser.add_argument(
            "-t",
            "--output-types",
            nargs="+",
            help="report output type",
            choices=list(OUTPUT_TYPES.keys()),
            required=True,
        )
        parser.add_argument(
            "-d",
            "--deidentify",
            action="store_true",
            help="Deidentify PHI elements in reports",
        )
        return parser.parse_args(args)

    def _get_sample(self, sample_id):
        sample = self.module_loader.load_module(self.config.get("sample_module"))
        sample_def = get_sample_definition(self.config, self.args.fcid, sample_id)
        return sample(sample_def, self.args.fcid, self.config)

    def run(self):
        pre_reports = list()
        for sample_id in self.args.sample_ids:
            sample = self._get_sample(sample_id)
            vip_variants = sample.get_vip_variants()
            sample_codex = dict()
            self._get_mock_codex_data(vip_variants, sample_codex)
            snv_variants = self._filter_reportable_variants(vip_variants, sample_codex)
            print("VIP variants:", len(vip_variants))
            print("Sample Codex Data:", sample_codex)
            print("Filtered SNV variants", len(snv_variants))
            report_data = self.data_builder.initialize_data(
                snv_variants, sample_codex, sample
            )
            filename_prefix = self.get_filename_prefix(
                sample, report_data["isPreliminary"], report_data["overallInterpretation"]
            )
            for formatter in self.output_formatters:
                initial_data = copy.deepcopy(report_data)
                pre_reports.append(
                    formatter.format_output(initial_data, sample, filename_prefix)
                )
        return pre_reports

    def _get_mock_codex_data(self, vip_variants, sample_codex):
        for variant in vip_variants:
            infoFields = parse_vcf_info(variant.info)
            path = ""
            if "emerge_category" in infoFields:
                path = infoFields["emerge_category"]
            elif "gene" in infoFields and infoFields["gene"] == "HFE":
                path = "path"
            if path.strip().lower() == "path" or path.strip().lower() == "lpath":
                sample_codex[variant.key()] = "Heterozygous"

    def _filter_reportable_variants(self, neptuneVariants, sampleCodexData):
        filteredVariants = list()
        for sv in neptuneVariants:
            print(sv.key())
            path = sv.get_classification()
            if sv.key() in sampleCodexData:
                if path in ["RISK ALLELE", "LPATH", "PATH", "PATHOGENIC"]:
                    filteredVariants.append(sv)
        return filteredVariants

    def get_filename_prefix(self, sample, is_preliminary, interpretation):
        filename = "{dir}/{patientID}.{sampleID}.{finding}{isPreliminary}$ADDENDA_TEXT.preReport.{lane_barcode}"
        return filename.format(
            dir=sample.output_dir,
            patientID=sample.get_patient_id(),
            sampleID=sample.internal_id,
            finding="POS" if interpretation == "Positive" else "NEG",
            isPreliminary=".preliminary" if is_preliminary else "",
            lane_barcode=sample.lane_barcode,
        )
