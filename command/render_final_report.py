import csv
import datetime
import re
from time import strftime
import argparse
from config.config import PROJECT_CONFIGS, OUTPUT_TYPES
from utils.neptune_util import run_cmd, makedir, get_sample_definition
from modules.module_loader import ModuleLoader
from utils.db_util import DBUtil


class RenderFinalReport:
    """
    Populates the pre-report with the final set of data; separate in case this needs to run in a PHI environment
    """

    def __init__(self, args, config, module_loader):
        self.args = self._setup_args(args)
        config.load_project_config(self.args.project)
        config.load_output_types_config(self.args.output_types)
        self.config = config
        self.module_loader = module_loader
        self.upload_location = config.get("upload", "location")
        self.access_token = config.get("upload", "access_token")
        self.module_loader = ModuleLoader()
        self.test_requisition = self.args.requisition
        self.output_formatters = list()
        for output in self.args.output_types:
            output_formatters = self.module_loader.load_module(
                config.get(output + "_module")
            )
            self.output_formatters.append(output_formatters(config, DBUtil(config)))

    def _setup_args(self, args):
        parser = argparse.ArgumentParser(
            prog='render-final-report',
            description=self.__doc__.strip()
        )
        parser.add_argument(
            "-p",
            "--project",
            metavar="PROJECT",
            help="project type",
            choices=list(PROJECT_CONFIGS.keys()),
            required=True,
        )
        parser.add_argument(
            "-f",
            "--fcid",
            help="Flowcell ID to run",
            required=True,
        )
        parser.add_argument(
            "-i",
            "--sample-ids",
            nargs="+",
            metavar='SAMPLE_ID',
            help="One or more sample IDs in the sample flowcell path",
            required=True,
        )
        parser.add_argument(
            "-t",
            "--output-types",
            nargs="+",
            help="report output type",
            choices=list(OUTPUT_TYPES.keys()),
            required=True,
        )
        parser.add_argument(
            "-r",
            "--requisition",
            help="Test requisition (.csv) that contains PHI",
            required=True,
        )
        return parser.parse_args(args)

    def _get_sample(self, sample_id):
        sample = self.module_loader.load_module(self.config.get("sample_module"))
        sample_def = get_sample_definition(self.config, self.args.fcid, sample_id)
        return sample(sample_def, self.args.fcid, self.config)

    def run(self):
        final_reports = list()
        for sample_id in self.args.sample_ids:
            sample = self._get_sample(sample_id)
            filename_prefix = sample.output_dir + "/" + "*preReport*"
            phi_data = self._load_phi_data(sample.internal_id, self.test_requisition)
            for formatter in self.output_formatters:
                final_reports.append(
                    formatter.format_phi_output(phi_data, filename_prefix)
                )
        return final_reports

    def upload(self, reports, output_dir):
        pass  # upload using access token

    def _load_phi_data(self, internal_id, test_requisition):
        phi_data = {"fileName": test_requisition}
        with open(test_requisition) as f:
            for i, row in enumerate(csv.reader(f)):
                if i == 0:
                    phi_data["PHYSICIAN_NAME"] = row[7]
                elif i > 2:  # skip header rows
                    if internal_id == row[1]:
                        self._parse_phi_data(phi_data, row, internal_id)
                        break
        return phi_data

    def _parse_phi_data(self, phi_data, row, internal_id):
        phi_data.update(
            {
                "PATIENT_NAME": " ".join((row[2], row[3], row[4])),
                "PATIENT_DOB": row[5],
                "PATIENT_AGE": row[6],
                "PATIENT_ID": row[7],
                "PATIENT_GENDER": row[8],
                "PATIENT_SAMPLE_ID": row[17],
                "PATIENT_ACCESSION": internal_id,
                "REPORT_DATE": strftime("%m/%d/%Y"),
                "SAMPLE_COLLECTED_DATE": row[20],
                "SAMPLE_TYPE": row[18],
                "DISEASE_NAME": row[11],
            }
        )
        return phi_data
