import json
import os
import re
import glob
import difflib
from components.default_project.sample import SampleVariant
from utils.db_util import DBUtil
from command.annotate import Annotate
from command.render_pre_report import RenderPreReport
from modules.module_loader import ModuleLoader

VALIDATE_PROJECT = 'default'
VALIDATE_SAMPLE_IDS = ['-i', '100060', '100204']
VALDIATE_OUTPUT_TYPES = ['-t', 'default', 'html', 'fhir']


class Validate:
    """
    Runs Annotate and RenderPreReport commands on example samples; compares outputs to a previously saved set of passing reports
    """

    def __init__(self, args, config, module_loader):
        config.load_project_config(VALIDATE_PROJECT)
        # print(config.to_config())
        self.config = config
        self.fcid = self.config.get('test', 'flowcell_id')
        self.module_loader = module_loader
        self.db_util = DBUtil(self.config)
        self.module_loader = ModuleLoader()

    def run(self):
        annotate_args = ['-p', VALIDATE_PROJECT, '-f', self.fcid] + VALIDATE_SAMPLE_IDS
        annotate = Annotate(annotate_args, self.config, self.module_loader)
        render_prereport_args = ['-p', VALIDATE_PROJECT, '-f', self.fcid] + VALIDATE_SAMPLE_IDS + VALDIATE_OUTPUT_TYPES
        render_pre_report = RenderPreReport(render_prereport_args, self.config, self.module_loader)
        json_reports = list()
        html_reports = list()
        annotation_paths = annotate.run()
        report_paths = render_pre_report.run()
        for path in report_paths:
            if path.endswith(".json"):
                json_reports.append(path)
            elif path.endswith(".html"):
                html_reports.append(path)
        confirmed_cnvs = self.db_util.get_confirmed_cnvs(self.fcid)
        self._run_tests(annotation_paths, confirmed_cnvs, json_reports, html_reports)

    def _run_tests(self, vip_annotations, confirmedCNVs, jsons, reports):
        comparisons = list()
        comparisons.append(
            self._compare_file(
                vip_annotations,
                self.config.get("test", "vip_annotation_dir"),
                "VIP annotations",
            )
        )
        comparisons.append(
            self._compare_json(jsons, self.config.get("test", "json_dir"))
        )
        comparisons.append(
            self._compare_file(reports, self.config.get("test", "html_dir"), "HTMLs")
        )
        self._print_summary(
            comparisons,
            "Validation File Comparisons",
            ["Files", "# Pass", "# Missing", "# Total", "Result"],
        )

    def _get_test_result(self, passingCases, total):
        return "pass" if passingCases == total else "FAIL"

    def _print_summary(self, testResults, title, columns):
        print("_" * 40, "\n", " " * 9, title)
        print("\t".join(columns))
        print("_" * 40)
        for res in testResults:
            print("\t".join(res))

    def ordered(self, obj):
        if isinstance(obj, dict):
            return sorted((k, self.ordered(v)) for k, v in list(obj.items()))
        if isinstance(obj, list):
            return sorted(self.ordered(x) for x in obj)
        else:
            return obj

    def _compare_json(self, jsons, validation_base_path):
        correct_count, wrong_count, missing_count = 0, 0, 0
        for test_json in jsons:
            filename = os.path.basename(test_json)
            expected_json = "%s%s" % (validation_base_path, filename)
            if not os.path.exists(expected_json):
                print("WARNING: %s does not exist" % expected_json)
                missing_count += 1
                continue
            with open(test_json) as f:
                test_lines = self.ordered(json.load(f))
            with open(expected_json) as f:
                expected_lines = self.ordered(json.load(f))
            if test_lines == expected_lines:
                print("Jsons match for %s, %s" % (test_json, expected_json))
                correct_count += 1
            else:
                wrong_count += 1
                print("Jsons do NOT match for %s, %s" % (test_json, expected_json))
                # split json objs into lines in order to diff
                expected_lines = str(expected_lines).split(", (")
                test_lines = str(test_lines).split(", (")
                self._diff_results(expected_lines, test_lines)
        total = len(jsons)
        print("Json comparison complete: %d incorrect" % wrong_count)
        return [
            "JSONs",
            str(correct_count),
            str(missing_count),
            str(total),
            self._get_test_result(correct_count, total),
        ]

    def _compare_file(self, files, validation_base_path, test_name):
        correct_count, wrong_count, missing_count, total = 0, 0, 0, len(files)
        for test_file in files:
            filename = os.path.basename(test_file)
            expected_file = validation_base_path + filename
            if not os.path.exists(expected_file):
                print("WARNING: %s does not exist" % expected_file)
                missing_count += 1
                continue
            with open(expected_file) as f:
                expected_lines = f.readlines()
            with open(test_file) as f:
                test_lines = f.readlines()
            if expected_lines == test_lines:
                print("Files match for %s, %s" % (test_file, expected_file))
                correct_count += 1
            else:
                print("Files do NOT match for %s, %s" % (test_file, expected_file))
                wrong_count += 1
                self._diff_results(expected_lines, test_lines)
        print("File comparisons completed: %d incorrect" % wrong_count)
        return [
            test_name,
            str(correct_count),
            str(missing_count),
            str(total),
            self._get_test_result(correct_count, total),
        ]

    def _diff_results(self, expectedLines, testLines):
        diff = difflib.ndiff(expectedLines, testLines)
        for line in list(diff):
            if not line.startswith(" "):  # show changed lines only
                print(line)
