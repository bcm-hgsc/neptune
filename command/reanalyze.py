import argparse
from datetime import datetime


class Reanalyze:
    """
    Takes an existing VIP annotated vcf and tags differences to current VIP
    """

    def __init__(self, args, config, module_loader):
        args = self._setup_args(args)
        self.config = config
        self.module_loader = module_loader
        self.new_vip = args.new_vip

    def _setup_args(self, args):
        parser = argparse.ArgumentParser(
            prog='reanalyze',
            description=self.__doc__.strip()
        )
        parser.add_argument(
            "-v",
            "--new-vip",
            help="Newer version of VIP to compare against the current config's VIP",
            required=True,
        )
        return parser.parse_args(args)

    # sample vip line:
    # CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	SAMPLE
    # 1	876547	.	G	A	99	PASS	M_ref=eMerge_970470;Reference=SAMD11:g.876547G>A;Gene=SAMD11;chr=1;genomic=g.876547G>A;Location=Exon 8;Nucleotide_corrected=c.730G>A;AminoAcid=p.Val244Met;exac=2/58958;polyphen2=D;COMPARISON=Baylor only;eMerge_category=CodifiedReview	GT:GL:GOF:GQ:NR:NV	1/1:-57.4,-6.32,0.0:14:63:20:20

    # note that variants in the VIP need to be normalized

    # testing
    # test 1: running with exact sample VIP gives no differences
    # test 2: creating one updated VIP gives correct difference

    def run(self):
        current_vip = self.config.get('vip_file')
        # TODO if clinvar is specified, use them to find the new assertions

        current_vip_data = self.parse_vip(current_vip)
        update_vip_data = self.parse_vip(self.new_vip)
        # Look through the current VIP, mark variants that have updated with a change tag
        # annotate variants in updated VIP that have changed
        for var_key in current_vip_data:
            var_data = current_vip_data[var_key]
            if var_key in update_vip_data:
                updated_vip_entry = self._update_vip_entry(
                    var_data, update_vip_data[var_key]
                )
                print("\t".join(updated_vip_entry) + "\n")
            else:
                print(var_data[-1] + "\n")

    def _build_key(self, chrom, pos, ref, alt, info=None):
        if info is not None:
            g_dot = self.find_field(info.split(";"), "Reference")[1]
            var_key = "|".join((chrom, pos, ref, alt, g_dot))
            return var_key
        else:
            return "|".join((chrom, pos, ref, alt))

    def parse_vip(self, vip):
        vip_data = dict()  # key is var_key, value is tuple of vcf fields + the line
        with open(vip) as f:
            for line in f:
                if not line.startswith("#"):
                    (
                        chrom,
                        pos,
                        varid,
                        ref,
                        alt,
                        qual,
                        var_filter,
                        info,
                        data_format,
                        sample,
                    ) = line.strip().split("\t")
                    var_key = self._build_key(chrom, pos, ref, alt, info)
                    vip_data[var_key] = (
                        chrom,
                        pos,
                        varid,
                        ref,
                        alt,
                        qual,
                        var_filter,
                        info,
                        data_format,
                        sample,
                    )
        return vip_data

    def parse_clinvar(self, clinvar):
        # CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
        cv_data = dict()  # key is var_key, value is tuple of vcf fields + the line
        with open(clinvar) as f:
            for line in f:
                if not line.startswith("#"):
                    (
                        chrom,
                        pos,
                        varid,
                        ref,
                        alt,
                        qual,
                        var_filter,
                        info,
                    ) = line.strip().split("\t")
                    var_key = self._build_key(chrom, pos, ref, alt)
                    assert var_key not in cv_data
                    cv_data[var_key] = (
                        chrom,
                        pos,
                        varid,
                        ref,
                        alt,
                        qual,
                        var_filter,
                        info,
                    )
        return cv_data

    def _update_vip_entry(self, current_entry, updated_entry):
        """
        Detect change to the emerge category.
        Update the category in the current_vip to the updated one
        And add a tag showing the change
        """
        # TODO rename emerge_category
        INFO_POS = 7
        updated_info = updated_entry[INFO_POS]
        current_info = current_entry[INFO_POS]
        new_info = self._update_info(current_info, updated_info)
        rv = list(current_entry)
        rv[7] = new_info
        return rv

    def find_field(self, fields, field_to_find):
        for field in fields:
            if field.startswith(field_to_find):
                return field.split("=")
        assert False, "unable to find " + field_to_find + " in " + fields

    def _update_info(self, current_info, updated_info):
        # an info tag is a semicolon separated string
        # find the previous emerge category,
        # Note that the change time is when the change was detected
        # TODO what if there already is a change tag?
        change_tag = ""
        updated_fields = updated_info.split(";")
        current_fields = current_info.split(";")
        current_emerge_category = self.find_field(current_fields, "eMerge_category")[1]
        updated_emerge_category = self.find_field(updated_fields, "eMerge_category")[1]
        if current_emerge_category == updated_emerge_category:
            return updated_info
        else:
            now = datetime.now()
            change_tag = "change=%s" % now.strftime("%Y-%m-%d")
            change_tag += "|previous_category=" + current_emerge_category
            return updated_info + ";" + change_tag
