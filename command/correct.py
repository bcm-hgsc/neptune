import argparse
from utils.db_util import DBUtil


class Correct:
    """
    Adds report correction note to the Neptune sqlite3 database
    """

    def __init__(self, args, config, module_loader):
        args = self._setup_args(args)
        self.db_util = DBUtil(config)
        self.fcid = args.fcid
        self.sample_id = args.sample_id
        self.reason = args.reason
        self.detail = args.detail

    def _setup_args(self, args):
        parser = argparse.ArgumentParser(
            prog='correct',
            description=self.__doc__.strip()
        )
        parser.add_argument(
            '-f',
            '--fcid',
            help='Flowcell ID of the target report',
            required=True,
        )
        parser.add_argument(
            '-i',
            '--sample-id',
            help='Sample ID of the target report',
            required=True,
        )
        parser.add_argument(
            '-r',
            '--reason',
            help='Reason for the correction',
            required=True,
        )
        parser.add_argument(
            '-d',
            '--detail',
            help='A detailed explanation of the reason; if not provided, Neptune will use the same value as the reason text',
        )
        return parser.parse_args(args)

    def run(self):
        self.db_util.add_correction(self.fcid, self.sample_id, self.reason, self.detail)
