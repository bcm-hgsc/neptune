import os
import csv
import re
import argparse
from components.vip import VIP
from utils.neptune_util import (
    parse_gene_list,
    get_key,
    parse_vcf_info,
    get_classification,
    get_sample_definition
)
from config.config import PROJECT_CONFIGS
from components.default_project.sample import SampleVariant
from utils.neptune_util import get_key, makedir


class Annotate:
    """
    Annotates vcf with data from the VIP. Marks if a variant has been previously seen or needs review
    """

    def __init__(self, args, config, module_loader):
        self.args = self._setup_args(args)
        config.load_project_config(self.args.project)
        self.config = config
        self.module_loader = module_loader
        self.reportable_genes = parse_gene_list(self.config, "reportable_genes")
        self.pathSNPVariants = dict()  # key is chrom|pos, value is snpInfo object
        snp_list_file = config.get("snp_list")
        with open(snp_list_file) as f:
            for row in csv.reader(f):
                if row[0] == "Gene":
                    continue
                snpInfo = SNPInfo(row)
                self.pathSNPVariants[get_key(snpInfo.chrom, snpInfo.pos)] = snpInfo

    def _setup_args(self, args):
        parser = argparse.ArgumentParser(
            prog='annotate',
            description=self.__doc__.strip()
        )
        parser.add_argument(
            "-p",
            "--project",
            metavar="PROJECT",
            help="project type",
            choices=list(PROJECT_CONFIGS.keys()),
            required=True,
        )
        parser.add_argument(
            "-f",
            "--fcid",
            help="Flowcell ID to run",
            required=True,
        )
        parser.add_argument(
            "-i",
            "--sample-ids",
            nargs="+",
            metavar='SAMPLE_ID',
            help="One or more sample IDs in the sample flowcell path",
            required=True,
        )
        return parser.parse_args(args)

    def _get_sample(self, sample_id):
        sample = self.module_loader.load_module(self.config.get("sample_module"))
        sample_def = get_sample_definition(self.config, self.args.fcid, sample_id)
        return sample(sample_def, self.args.fcid, self.config)

    def run(self):
        fc_dir = self.config.get("fc_path").format(flowcell=self.args.fcid)
        makedir(fc_dir)
        vipFileName = self.config.get("vip_file")
        print("USING VIP: ", vipFileName)
        self._set_vip_variants(vipFileName)
        annotations = list()
        for sample_id in self.args.sample_ids:
            sample = self._get_sample(sample_id)
            annotations.append(self.filter(sample))
        return annotations

    def _set_vip_variants(self, vip_file):
        vip = VIP(vip_file, self.config)
        self.path_vip_variants = vip.path_vip_variants
        self.vus_variants = vip.vus_variants
        self.vip_file = os.path.split(vip_file)[1]

    def filter(self, sample):
        sample_id = sample.internal_id
        log = open("%s/neptune.log" % sample.output_dir, "a")
        log.write("\n\nversion: %s\n" % self.config.get("version"))
        log.write("\n\nsample: %s\n" % sample.internal_id)
        annotate_output = "%s/%s_%s-%s.%s.neptune.vcf"
        annotate_output = annotate_output % (
            sample.output_dir,
            sample_id,
            sample.fcID,
            sample.lane_barcode,
            self.vip_file.replace(".vcf", ""),
        )
        print(annotate_output)
        out = open(annotate_output, "w")
        out.write("##source=%s\n" % self.config.get("version"))
        out.write("#VIPDatabase:%s\n" % self.vip_file.replace(".vcf", ""))
        log.write("#VIPDatabase:%s\n" % self.vip_file.replace(".vcf", ""))
        out.write("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	%s\n" % sample_id)
        # these genes need two hits if they're not homozygous
        ar_genes = ["MUTYH"]
        ar_snp_genes = ["MEFV", "BCKDHB"]

        gene_hits, snp_hits = dict(), dict()
        results = list()
        # filter the inputVCF for variants in the vipList
        # append the vip info column to the info column in the inputVCF
        with open(sample.mab_path) as f:
            for line in f:
                if line.startswith("CHROM"):
                    continue
                if line.startswith("#"):
                    out.write(line)
                row = line.strip().split()
                # CHROM   POS REF ALT QUAL    FILTER  FORMAT  Pileup_data._NB:for_compliance_with_the_VCF_standard_;_has_been_replaced_with_:_(59-58) Mutation_type_(Refseq)  Mutation_type_(UCSC)
                (
                    chrom,
                    pos,
                    ref,
                    alt,
                    qual,
                    filt,
                    data,
                    pileup,
                    mtRS,
                    mtUCSC,
                    gene,
                    annovarData,
                ) = row[0:12]
                rs_number = row[55]
                in_path_snp_variants = get_key(chrom, pos) in self.pathSNPVariants
                if gene not in self.reportable_genes and not in_path_snp_variants:
                    continue
                # Add some MAB columns to the output
                mabInfo = (
                    "format=%s;pileup=%s;mutationTypeRefSeq=%s;mutationTypeUCSC=%s;annovarData=%s;dbSNP_rsid=%s"
                    % (data, pileup, mtRS, mtUCSC, annovarData, rs_number)
                )
                if filt.strip().upper() != "PASS":
                    continue
                # for the snps where we need to see a comp het, keep track of them for later analysis
                if gene in ar_snp_genes:
                    if gene not in snp_hits:
                        snp_hits[gene] = list()
                    snp_hits[gene].append(line)

                if in_path_snp_variants:
                    snpInfo = self.pathSNPVariants[get_key(chrom, pos)]
                    # A SNP is path if annovar has the same cDNA change
                    if self._is_path_snp(
                        snpInfo, chrom, pos, ref, alt, self._get_zygosity(data)
                    ):
                        info = self._build_info(snpInfo.info, mabInfo)
                        print("PATH SNP:", chrom, pos)
                        info = self._build_info(
                            info, "isSNP=True;sampleID=%s" % sample_id
                        )
                        # If the SNP is in the VIP (which contains the reviewer's comments, get those fields as well
                        if get_key(chrom, pos, ref, alt) in self.path_vip_variants:
                            vipInfo = self.path_vip_variants[
                                get_key(chrom, pos, ref, alt)
                            ]
                            info = self._build_info(info, vipInfo)
                        variant = SampleVariant(
                            sample_id, chrom, pos, ref, alt, qual, filt, info, data
                        )
                        results.append(variant)
                        log.write(
                            "\t".join((chrom, pos, ".", ref, alt, info, data) + "\n")
                        )
                        print(chrom, pos, ".", ref, alt, "PATH SNP", gene)
                    else:
                        log.write(
                            "ALMOST SNP: %s\n"
                            % "\t".join((chrom, pos, ".", ref, alt, "", data))
                        )
                        print(chrom, pos, ".", ref, alt, "ALMOST SNP")
                elif get_key(chrom, pos, ref, alt) in self.path_vip_variants:
                    # if the gene is MUTYH, we need 2 hits in the same sample
                    if gene in ar_genes:
                        if gene not in gene_hits:
                            gene_hits[gene] = list()
                        gene_hits[gene].append(line)
                        continue
                    info = self.path_vip_variants[get_key(chrom, pos, ref, alt)]
                    path = get_classification(info)
                    info = self._build_info(info, mabInfo)
                    info = self._build_info(info, "sampleID=%s" % sample_id)
                    variant = SampleVariant(
                        sample_id, chrom, pos, ref, alt, qual, filt, info, data
                    )
                    results.append(variant)
                    log.write(
                        "%s\n" % "\t".join((chrom, pos, ".", ref, alt, info, data))
                    )
                    print(
                        chrom, pos, ".", ref, alt, "PATH VIP VARIANT", gene, path, data
                    )
                    if info.find("Harmonization") > 0:
                        print("^^^ HARMONIZATION!")
                elif get_key(chrom, pos, ref, alt) in self.vus_variants:
                    info = self.vus_variants[get_key(chrom, pos, ref, alt)]
                    info = self._build_info(info, mabInfo)
                    info = self._build_info(info, "sampleID=%s" % sample_id)
                    variant = SampleVariant(
                        sample_id, chrom, pos, ref, alt, qual, filt, info, data
                    )
                    results.append(variant)
                    print(chrom, pos, ".", ref, alt, "VUS_START VARIANT")

        # SPECIAL CASE 1: HFE variant - it is common, so the Mendelian afterburner filters it out
        with open(sample.get_snp_annotation()) as f:
            for line in f:
                if line.startswith("CHROM") or line.startswith("#"):
                    continue
                row = line.strip().split("\t")
                chrom, pos, id, ref, alt, qual, filt, info, format, sampleData = row
                # Find the variant
                if chrom == "6" and pos == "26093141" and ref == "G" and alt == "A":
                    log.write(
                        "%s\n" % "\t".join((chrom, pos, ".", ref, alt, "HFE Variant"))
                    )
                    print(chrom, pos, ".", ref, alt, "HFE Variant")
                    snpInfo = self.pathSNPVariants[get_key(chrom, pos)]

                    # A SNP is path if annovar has the same cDNA change
                    if self._is_path_snp(
                        snpInfo, chrom, pos, ref, alt, self._get_zygosity(sampleData)
                    ):
                        info = self._build_info(snpInfo.info, mabInfo)
                        # If the SNP is in the VIP (which contains the reviewer's comments
                        # get those fields as well
                        if get_key(chrom, pos, ref, alt) in self.path_vip_variants:
                            vipInfo = self.path_vip_variants[
                                get_key(chrom, pos, ref, alt)
                            ]
                            info = self._build_info(info, vipInfo)

                        info = self._build_info(
                            info, "isSNP=True;sampleID=%s" % sample_id
                        )
                        variant = SampleVariant(
                            sample_id, chrom, pos, ref, alt, qual, filt, info, data
                        )
                        results.append(variant)
                        log.write(
                            "%s\n" % "\t".join((chrom, pos, ".", ref, alt, info, data))
                        )
                        print(chrom, pos, ".", ref, alt, "PATH HFE SNP", gene)

            # SPECIAL CASE 2: Recessive variants need two hits to be reported
            # examine the AR genes to see if there is enough evidence to report them:
            for gene in list(gene_hits.keys()):
                if (
                    len(gene_hits[gene]) > 2
                    or self._count_homozygous(gene_hits[gene]) > 1
                ):
                    results.extend(self._output_lines(gene_hits[gene], sample_id, log))
            hasBCKDHBVar1, hasBCKDHBVar2 = False, False
            hasMEFVVar1, hasMEFVVar2 = False, False
            for gene in list(snp_hits.keys()):
                for line in snp_hits[gene]:
                    row = line.strip().split()
                    annovarData = row[11]
                    if gene == "BCKDHB":
                        # need the c.548G>C and c.832G>A variants
                        if annovarData.find("c.G548C") > 0:
                            hasBCKDHBVar1 = True
                        if annovarData.find("c.G832A") > 0:
                            hasBCKDHBVar2 = True
                    elif gene == "MEFV":
                        # need the c.2177T>C and c.2080A>G
                        if annovarData.find("c.T2177C") > 0:
                            hasBCKDHBVar1 = True
                        if annovarData.find("c.A2080G") > 0:
                            hasBCKDHBVar2 = True

            if hasBCKDHBVar1 and hasBCKDHBVar2:
                results.extend(
                    self._output_lines(snp_hits["BCKDHB"], sample_id, log, True)
                )
            if hasMEFVVar1 and hasMEFVVar2:
                results.extend(
                    self._output_lines(snp_hits["MEFV"], sample_id, log, True)
                )
        # SPECIAL CASE 3: F5 leiden variant - in the hg19 reference, the pathogenic allele is present
        if sample.get_snp_f5_annotation() is not None:
            print("F5 file exists", sample.get_snp_f5_annotation())
            with open(sample.get_snp_f5_annotation()) as f:
                for line in f:
                    if line.startswith("CHROM") or line.startswith("#"):
                        continue
                    # g.169519049C>T
                    # 1   169519049   .   T   .   .   No_var  ReqIncl GT:VR:RR:DP:GQ  0/0:0:256:256:.
                    row = line.strip().split("\t")
                    chrom, pos, id, ref, alt, qual, filt, info, format, sampleData = row
                    if chrom == "1" and pos == "169519049":
                        print(chrom, pos, ".", ref, alt, "F5 Variant")
                        snpInfo = self.pathSNPVariants[get_key(chrom, pos)]
                        print("SNP INFO", snpInfo)

                        # A SNP is path if annovar has the same cDNA change
                        if self._is_path_f5_snp(
                            snpInfo,
                            chrom,
                            pos,
                            ref,
                            alt,
                            self._get_zygosity(sampleData),
                        ):
                            info = self._build_info(snpInfo.info, mabInfo)

                            # If the SNP is in the VIP (which contains the reviewer's comments, get those fields as well
                            if get_key(chrom, pos, ref, "T") in self.path_vip_variants:
                                vipInfo = self.path_vip_variants[
                                    get_key(chrom, pos, ref, "T")
                                ]
                                info = self._build_info(info, vipInfo)
                                print("\nVIP Info:")
                                print(vipInfo)

                            info = self._build_info(
                                info, "isSNP=True;sampleID=%s" % sample_id
                            )
                            print("\nInfo:\n", info)
                            # codified represents this variant as T>T for some reason
                            variant = SampleVariant(
                                sample_id, chrom, pos, ref, "T", qual, filt, info, data
                            )
                            results.append(variant)
                            log.write(
                                "%s\n"
                                % "\t".join((chrom, pos, ".", ref, alt, info, data))
                            )
                            print(
                                chrom,
                                pos,
                                ".",
                                ref,
                                alt,
                                "PATH F5 SNP",
                                "originalFilter:",
                                filt,
                                "originalQuality",
                                qual,
                            )
        for sampleVariant in results:
            out.write(str(sampleVariant))
        out.close()
        log.close()
        return annotate_output

    def _build_info(self, info, extra_info):
        if len(info) > 0:
            info += ";"
        info += extra_info
        return info

    def _get_zygosity(self, vcfData):
        zyg = "unknown"
        # assume atlas vcf format:
        print("Calculating zygosity", vcfData)
        if len(vcfData.split(":")) == 5:
            gt, vr, rr, dp, gq = vcfData.split(":")
            alleles = [int(a) for a in gt.split("/")]
            assert len(alleles) == 2, "more alleles than expected! %s" % gt
            if alleles[0] == 0 and alleles[1] > 0:
                zyg = "het"
            elif alleles[1] == 0 and alleles[0] > 0:
                zyg = "het"
            elif alleles[1] > 0 and alleles[0] > 0 and alleles[0] == alleles[1]:
                zyg = "hom"
            elif alleles[1] == 0 and alleles[0] == 0:
                zyg = "homRef"
            elif alleles[1] > 0 and alleles[0] > 0 and alleles[0] != alleles[1]:
                zyg = "biallelic"
        return zyg

    def _is_valid_zyg(self, reportDef, zygosity):
        # the 'report definition' string will look like this:
        # Yes (hom, biallelic)
        # Yes (biallelic)
        # Yes (hom only)
        # Yes
        # This method does not handle COMP. HET Variants that are spread across multiple rows in the .vcf
        print(reportDef)
        print(zygosity)
        if reportDef.upper().find("HOM") and (
            zygosity == "hom" or zygosity == "homRef"
        ):
            return True
        if reportDef.upper().find("BIALLELIC") and zygosity == "biallelic":
            return True
        if reportDef.upper().strip() == "YES":
            return True
        return False

    # TODO create a "report rule" object, loaded from the db (rename SNPInfo)
    # TODO create a sample variant object (or use that one we have already....)
    def _is_path_snp(self, snpInfo, chrom, pos, ref, alt, zyg):
        # TODO search for the rs number OR the genomic coordinate. If the variant is complex, lets be conservative and always return variants at this position
        # if the variant is an indel, only match on position
        print(snpInfo.chrom, snpInfo.pos, snpInfo.ref, snpInfo.alt, zyg)
        print(chrom, pos, ref, alt)
        return (
            snpInfo.chrom == chrom
            and snpInfo.pos == pos
            and ((snpInfo.ref == ref and snpInfo.alt == alt) or snpInfo.isIndel)
            and self._is_valid_zyg(snpInfo.report, zyg)
        )

    def _is_path_f5_snp(self, snpInfo, chrom, pos, ref, alt, zyg):
        # 1   169519049   .   T   .   .   No_var  ReqIncl GT:VR:RR:DP:GQ  0/0:0:256:256:.
        # this is how it will appear in Atlas output
        return (
            chrom == "1"
            and pos == "169519049"
            and ref == "T"
            and alt == "."
            and zyg.startswith("hom")
        )

    def _count_homozygous(self, lines):
        count = 0
        for line in lines:
            row = line.strip().split()
            chrom, pos, ref, alt, qual, filt, data = row[0:7]
            if self._get_zygosity(data) == "hom":
                count += 1
        return count

    def _output_lines(self, lines, sample_id, log, is_snp=False):
        results = list()
        for line in lines:
            row = line.strip().split("\t")
            chrom, pos, ref, alt, qual, filt, data = row[0:7]
            if is_snp:
                info = self.pathSNPVariants[get_key(chrom, pos)].info
            else:
                info = self.path_vip_variants[get_key(chrom, pos, ref, alt)]
            info = self._build_info(info, "sampleID=%s" % sample_id)
            m = SampleVariant(sample_id, chrom, pos, ref, alt, qual, filt, info, data)
            results.append(m)
            if log is not None:
                log.write(str(m))
        return results


class SNPInfo:
    """ This class is parsed from the SNPLIST file we recieve from Magalie"""

    def __init__(self, csvRow):
        columns = [
            "gene=",
            "transcript=",
            "rsNumber=",
            "gDNAPosition=",
            "genomicPosition=",
            "cDNA=",
            "aaChange=",
            "location=",
            "inheritance=",
            "classificationBCM=",
            "report=",
            "actionable=",
            "reportClinicalSites=",
            "omim=",
            "baylorInterpretationDisorder=",
            "BaylorInterpretationVariant=",
            "reference=",
            "internalCommentForEric=",
        ]
        self.gDNA = csvRow[3]
        self.genomic = csvRow[4]
        self.cDNA = csvRow[5]
        self.chrom, self.pos = self.gDNA.split(":")
        self.chrom = self.chrom.replace("chr", "")
        self.report = csvRow[10]
        self.transcript = csvRow[2]
        self.ref, self.alt, self.isIndel = self.parseRefAlt(self.genomic)
        assert self.report is not None
        assert (
            self.chrom
            in "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y".split(",")
        )
        assert re.match("\d+$", self.pos)
        self.info = ";".join(
            ["".join((c, self._get_safe_semicolon(v))) for c, v in zip(columns, csvRow)]
        )

    def _get_safe_semicolon(self, s):
        return s.replace(";", "%3B")

    def parseRefAlt(self, genomic):
        # If the variant is a SNV, return the ref and alt, else return None, None
        # g.76226846A>G
        m = re.match(r"g\.\d+(\w)>(\w)", genomic)
        if m:
            ref, alt = m.group(1), m.group(2)
            return ref, alt, False
        return None, None, True
