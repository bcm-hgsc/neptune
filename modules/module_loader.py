from importlib import import_module
from pathlib import Path


class ModuleLoader:
    def __init__(self):
        pass

    def load_module(self, module_name):
        class_name = (
            module_name.split(".")[-1].replace("_", " ").title().replace(" ", "")
        )
        plugin_module = import_module(module_name)
        return getattr(plugin_module, class_name)
