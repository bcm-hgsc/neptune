from config.config import CONFIGS_PATH
from pathlib import Path


class PolygenicRiskScore:
    def __init__(self, config, prs_config_path):
        self.config = config
        self.config.load_config(Path(CONFIGS_PATH, prs_config_path).absolute())

    def add_data(self, report_data, sample, **kwargs):
        self._build_prs_data(report_data, sample.cardio_risk_path)

    def _build_prs_data(self, data, cardio_risk_file):
        with open(cardio_risk_file) as f:
            for line in f:
                if "Risk Score" in line:
                    score = line.strip().split(",")[-1]
                elif "Risk Comment" in line:
                    finding = line.strip().split(",")[-1]
                    if finding == "Top 5%":
                        finding = "HIGH"
                    elif finding == "Normal":
                        finding = "INTERMEDIATE - NOT TOP 5%"
        if finding == "HIGH":
            summary = self.config.get("prs", "positive_summary")
        else:
            summary = self.config.get("prs", "negative_summary")
        data.update(
            {
                "polygenicRiskScore": score,
                "prsFinding": finding,
                "prsSummary": summary + " " + self.config.get("prs", "summary_trailer"),
                "prsRecommendation": self.config.get("prs", "recommendation"),
                "prsRecommendationDetail": self.config.get(
                    "prs", "recommendation_detail"
                ),
                "prsDisclaimer": self.config.get("prs", "disclaimer"),
            }
        )
