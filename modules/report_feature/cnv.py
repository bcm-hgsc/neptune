import glob
import re
from config.config import CONFIGS_PATH
from pathlib import Path
from utils.db_util import DBUtil
from utils.output_util import clean_text, get_inheritance_pattern, build_comment

class Cnv:
    def __init__(self, config, cnv_config_path):
        self.config = config
        self.config.load_config(Path(CONFIGS_PATH, cnv_config_path).absolute())
    
    def add_data(self, report_data, sample, **kwargs):
        self._build_cnv_data(report_data, sample, kwargs['gene_data'])

    def _build_cnv_data(self, data, sample, gene_data):
        confirmedCNVs = DBUtil(self.config).get_confirmed_cnvs(sample.fcID)
        failedCNVs = self._load_failed_cnvs(sample.fcID)
        sample.set_cnv_analysis_failed(sample.internal_id in failedCNVs)
        confirmedCNV = (
            confirmedCNVs[sample.internal_id]
            if sample.internal_id in confirmedCNVs
            else dict()
        )
        cnv_variants = self._get_cnvs(sample.get_cnv_path(), confirmedCNV)
        if not sample.cnv_analysis_failed:
            for atlasPCNVResult in cnv_variants:
                variant = self._build_cnv_variant(atlasPCNVResult, gene_data)
                data["variants"].append(variant)
        snv_positive = data['overallInterpretation'] == 'Positive'
        is_positive = self._recalculate_finding(snv_positive, cnv_variants)
        data['overallInterpretationText'] = build_comment(is_positive, data['isPreliminary'], self.config)
        data['overallInterpretation'] = "Positive" if is_positive else "Negative"
        data['cnvsFailed'] = sample.cnv_analysis_failed


    def _get_cnvs(self, cnv_file, confirmed_cnvs):
        cnvs = list()
        with open(cnv_file) as f:
            for line in f:
                if line.startswith("Exon_Target"):
                    continue
                entry = AtlasPCNVOutputEntry(line)
                print(entry)
                # The confirmed CNVs are manually entered into the tracking database on return of the MLPA results
                # Find the Atlas results that are described in the tracking db
                if entry.geneExon in confirmed_cnvs:
                    print("found", entry.geneExon)
                    entry.setConfirmedData(confirmed_cnvs[entry.geneExon])
                    cnvs.append(entry)
        return cnvs

    def _load_failed_cnvs(self, flowcell_id):
        cnvLogDir = (
            self.config.get("fc_path").format(flowcell=flowcell_id)
            + self.config.get('cnv_log_path')
        )
        cnvLogs = glob.glob(cnvLogDir)
        print("CNV LOGS:", cnvLogs)
        failedSamples = list()
        for file in cnvLogs:
            with open(file) as f:
                for line in f:
                    # 100088_HMCYNBCXX-1-IDMB87   0.22    3563    1623.82
                    if re.match(r"\w+_\w+-[12]-IDMB\d+", line):
                        def removePrefix(text, prefix):
                            return text[text.startswith(prefix) and len(prefix) :]
                        s = removePrefix(line.strip(), "sample_id")
                        failedSamples.append(s.split("\t")[0].split("_")[0])
                    elif (
                        line.find("IDMB") >= 0
                    ):  # we missed something that looks like it might be a sample identifier
                        assert False, line
        for entry in failedSamples:
            print("\t FAILED CNVs:", entry)
        return failedSamples

    def _build_cnv_variant(self, atlasPCNVResult, gene_data):
        variant = {
            "gene": atlasPCNVResult.gene,
            "variantCuration": atlasPCNVResult.interpString,
            "zygosity": atlasPCNVResult.zyg,
            "type": "CNV",
            "geneRegion": atlasPCNVResult.confirmedExons,
            "structuralChangeType": atlasPCNVResult.interp().upper(),
            "externalId": atlasPCNVResult.getCNVExternalID(),
            "categoryType": atlasPCNVResult.medicalRelevance,
            "transcript": atlasPCNVResult.transcript,
            "notInterpreted": False,
            "interrogatedButNotFound": False,
            "confirmedBySanger": False,
            "confirmedByMLPA": True
        }
        variant["interpretation"] = self._get_medical_relevance_interp(variant["categoryType"])
        diseaseText, disease, inheir = "", "", ""
        if atlasPCNVResult.gene in gene_data:
            diseaseText, disease, inheir, actionable = gene_data[atlasPCNVResult.gene] 
            diseaseText = diseaseText.strip()
            if len(diseaseText) > 0 and not diseaseText.endswith("."):
                diseaseText += "."
        variant["geneDiseaseText"] = clean_text(diseaseText)
        variant["disease"] = disease
        variant["diseases"] = list()
        if len(disease) > 0:
            variant["diseases"] = [s.strip() for s in disease.split(";")]
        variant["inheritance"] = inheir
        interpretation = "A %s %s in the %s gene was detected in this individual. %s %s This result was obtained from coverage analysis of a capture-sequencing test and confirmed by an MLPA assay." % \
            (variant["zygosity"].lower(), atlasPCNVResult.interpString, variant["gene"], atlasPCNVResult.variantInterpretation, variant["geneDiseaseText"])
        variant["variantInterpretation"] = " ".join([interpretation, get_inheritance_pattern(variant["inheritance"], self.config)])
        return variant

    def _get_medical_relevance_interp(self, medicalRelevance):
        if medicalRelevance == "MEDICALLY_SIGNIFICANT":
            return "Pathogenic"
        elif medicalRelevance == "UNKNOWN_SIGNIFICANCE" or medicalRelevance == "NOT_MEDICALLY_SIGNIFICANT":
            return "Uncertain Significance"

    def _recalculate_finding(self, snv_positive, cnv_variants):
        return snv_positive or len(cnv_variants) > self._count_vus_cnvs(cnv_variants)

    def _count_vus_cnvs(self, cnv_variants):
        count = 0
        if cnv_variants:
            for cnv in cnv_variants:
                if cnv.medicalRelevance in [
                    "UNKNOWN_SIGNIFICANCE",
                    "NOT_MEDICALLY_SIGNIFICANT",
                ]:
                    count += 1
        return count


class AtlasPCNVOutputEntry:
    def __init__(self, line):
        (
            self.exonTarget,
            self.geneExon,
            self.cnv,
            self.log2R,
            self.rpkm,
            self.median_rpkm,
            self.exonQCPF,
            self.exonQCValue,
            self.e_StDev,
            self.cScore,
        ) = ("", "", "", "", "", "", "", "", "", "")
        if len(line.strip().split()) == 8:
            # Atlas v1.0
            # Exon_Target         Gene_Exon   cnv log2R   rpkm    median_rpkm ExonQC_PF   ExonQC_value
            # 7:6013030-6013173   PMS2-001_15 dup 0.82    478 270 Fail    0.58
            (
                self.exonTarget,
                self.geneExon,
                self.cnv,
                self.log2R,
                self.rpkm,
                self.median_rpkm,
                self.exonQCPF,
                self.exonQCValue,
            ) = line.strip().split()
        elif len(line.strip().split()) == 10:
            # Atlas v1.1
            # Exon_Target Gene_Exon   cnv log2R   rpkm    median_rpkm Exon_Status E_StDev c_Score RefSeq
            (
                self.exonTarget,
                self.geneExon,
                self.cnv,
                self.log2R,
                self.rpkm,
                self.median_rpkm,
                self.exonQCPF,
                self.e_StDev,
                cScore,
                refseq,
            ) = line.strip().split()
        else:
            assert False, "Unknown format of atlas CNV output"

        self.rpkm = int(self.rpkm)
        if self.geneExon.count("-") == 1:
            self.gene, self.exon = self.geneExon.split("-")
        elif self.geneExon.count("-") == 2:
            split = self.geneExon.rindex("-")
            self.gene = self.geneExon[:split]
            self.exon = self.geneExon[split + 1 :]
        else:
            assert False, "Too many '-'s in geneExon!"
        self.exonNumber = int(
            self.exon.rstrip("abcdefghijklmnopqrstuvwxyz").split("_")[1]
        )  # rstrip here because Atlas pcnv adds a letter to the end of some exons
        self.interpString = self.interp()
        self.zyg = self.calcZygosity()
        self.chrom, startend = self.exonTarget.split(":")
        self.pos, self.endPos = startend.split("-")

    def __str__(self):
        return "\t".join(
            (
                self.exonTarget,
                self.geneExon,
                self.cnv,
                self.log2R,
                str(self.rpkm),
                self.median_rpkm,
                self.exonQCPF,
                self.exonQCValue,
                self.e_StDev,
                self.cScore,
            )
        )

    def getCNVExternalID(self):
        return "%s,%s,%s" % (self.chrom, self.pos, self.endPos)

    def calcZygosity(self):
        if self.cnv == "dup":
            return ""
        if self.cnv == "del":
            if self.rpkm < 1:
                return "Homozygous"
            return "Heterozygous"

    def interp(self):
        if self.cnv == "dup":
            return "duplication"
        if self.cnv == "del":
            return "deletion"

    def setStartEndExon(self, startExon, endExon):
        # Used when combining multiple contiguous cnv calls into a single call in the report generator
        self.startExon = startExon
        self.endExon = endExon

    def setConfirmedData(self, confirmed_data):
        self.transcript = confirmed_data['transcript']
        self.confirmedExons = confirmed_data['confirmed_exons']
        self.medicalRelevance = confirmed_data['medical_relevance']
        self.variantInterpretation = confirmed_data['variant_interpretation']
        if self.variantInterpretation is None:
            self.variantInterpretation = ""