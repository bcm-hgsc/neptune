from config.config import CONFIGS_PATH
from pathlib import Path


class Lpa:
    def __init__(self, config, lpa_config_path):
        self.config = config
        self.config.load_config(Path(CONFIGS_PATH, lpa_config_path).absolute())
        self.lpa_genes = self.config.get("lpa_genes")

    def add_data(self, report_data, sample, **kwargs):
        self._build_lpa_data(report_data, sample.get_snp_gvcf())

    def _build_lpa_data(self, data, gvcf):
        lpa = self._get_lpa_variants(gvcf)
        data["lpaDisclaimer"] = self.config.get("lpa", "disclaimer")
        if lpa:
            if len(lpa) == 1:
                lpaSummary = self.config.get("lpa", "positive_summary_single").format(
                    gene1=lpa[0]
                )
            else:
                lpaSummary = self.config.get("lpa", "positive_summary_double").format(
                    gene1=lpa[0], gene2=lpa[1]
                )
            data.update(
                {
                    "lpaFinding": "POSITIVE",
                    "lpaSummary": lpaSummary
                    + " "
                    + self.config.get("lpa", "positive_summary_trailer"),
                    "lpaRecommendation": self.config.get(
                        "lpa", "positive_recommendation"
                    ),
                    "lpaRecommendationDetail": self.config.get(
                        "lpa", "positive_recommendation_detail"
                    ),
                }
            )
        else:
            data.update(
                {
                    "lpaFinding": "NEGATIVE",
                    "lpaSummary": self.config.get("lpa", "negative_summary"),
                    "lpaRecommendation": self.config.get(
                        "lpa", "negative_recommendation"
                    ),
                    "lpaRecommendationDetail": self.config.get(
                        "lpa", "negative_recommendation_detail"
                    ),
                }
            )

    def _get_lpa_variants(self, gvcf):
        lpa = list()
        with open(gvcf) as f:
            for line in f:
                if line.startswith("CHR") or line.startswith("#"):
                    continue
                (
                    chrom,
                    pos,
                    ref,
                    alt,
                    qual,
                    filt,
                    info,
                    data,
                    genotype,
                ) = self._parse_line(line)
                # if the row is a variant, check if the position is in the pgx list
                if self._is_variant(alt, qual, info):
                    if chrom in self.lpa_genes and pos in self.lpa_genes[chrom]:
                        lpa_dict = self.lpa_genes[chrom][pos]
                        if (
                            ref == lpa_dict["ref"]
                            and alt == lpa_dict["alt"]
                            and genotype != "0/0"
                        ):
                            lpa.append(lpa_dict["gene"])
        return lpa

    # and a coverage block looks like this:
    # 1   902885  .   A   .   .   PASS    END=913459;BLOCKAVG_min30p3a    GT:VR:RR:DP:GQ:PL:VRX:RRX:DPX   ./.:0:0:0:.:0,27,30:0,0,0,0:0,0,0,0:0,0,0,0
    # a variant row looks like this:
    # 1   902773  .   T   C   1   low_snpqual;low_coverage;low_VariantReads;low_VariantRatio  P=0.281621  GT:VR:RR:DP:GQ:PL   0/0:1:1:2:.:0,4,29
    def _is_variant(self, alt, qual, info):
        return alt.isalpha() and qual.isdigit() and info.startswith("P")

    def _parse_line(self, line):
        row = line.strip().split("\t")
        chrom, pos, id, ref, alt, qual, filt, info, format, data = row[0:10]
        genotype = data.split(":")[0]
        assert genotype in ("0/0", "0/1", "1/0", "1/1", "./."), genotype
        return chrom, pos, ref, alt, qual, filt, info, data, genotype
