import pickle
import re
from config.config import CONFIGS_PATH
from pathlib import Path


class PgxResult:
    def __init__(self, gene, position, ref, alt, gt):
        self.gene = gene
        self.position = position
        self.alt = alt
        self.ref = ref
        self.gt = gt

    def __repr__(self):
        return " ".join([self.gene, self.position, self.ref, self.alt, self.gt])

    def getGenotype(self):
        geno = None
        if self.gt == "0/0":
            geno = self.ref, self.ref
        if self.gt == ".":
            geno = self.ref, self.ref
        if self.gt == "0/1":
            geno = self.ref, self.alt
        if self.gt == "1/1":
            geno = self.alt, self.alt
        assert geno is not None
        return "".join(geno)


class Pgx:
    def __init__(self, config, pgx_config_path):
        self.config = config
        self.config.load_config(Path(CONFIGS_PATH, pgx_config_path).absolute())
        self.PGXList = dict()  # key is chrom|pos
        with open(config.get("pgx", "pgx_list")) as f:
            for line in f:
                row = line.strip().split("\t")
                chrom = row[0].replace("chr", "").strip()
                pos = row[1].strip()
                gene = row[2].strip()
                self.PGXList[self.get_key(chrom, pos)] = gene
        self.pgxData = pickle.load(open(config.get("pgx", "pgx_data"), "rb"))

    def add_data(self, report_data, sample, **kwargs):
        self._build_pgx_data(report_data, sample.get_pgx_path())
        report_data[
            "pgxDescription"
        ] = "Pharmacogenomics variants are returned for the following genes: %s. " % (
            self.config.get("pgx", "genes")
        ) + self.config.get(
            "pgx", "description"
        )

    def _build_pgx_data(self, data, pgx_path):
        print("PGx DATA FILE:", pgx_path)
        results = dict()
        with open(pgx_path) as f:
            for line in f:
                if line.startswith("CHROM") or line.startswith("#"):
                    continue
                chrom, pos, ref, alt, genotype = self._parse_line(line)
                gene = self.PGXList[self.get_key(chrom, pos)]
                if gene not in results:
                    results[gene] = list()
                pgxr = PgxResult(gene, pos, ref, alt, genotype)
                print(gene, pgxr)
                results[gene].append(pgxr)
        data["initialPgxData"] = dict()
        for gene in results:
            data["initialPgxData"][gene] = self.getStarAllele(gene, results[gene])

    def get_key(self, chrom, pos):
        return "%s|%s" % (chrom, pos)

    def getInterp(self, d, key):
        if key in d:
            return d[key]
        return "N/A", "N/A"

    def noValNA(self, d, key):
        if key in d:
            return d[key]
        return "N/A"

    def formatDT(self, dt):
        assert len(dt) == 2
        return "%s/%s" % (dt[0], dt[1])

    def getStarAllele(self, gene, res):
        diplotypeKey = "".join(
            [
                pgxRes.getGenotype()
                for pgxRes in sorted(res, key=lambda res: res.position)
            ]
        )
        if gene == "IFNL3":
            phenotype, interp = self.getInterp(
                self.pgxData["IFNL3Interp"], diplotypeKey
            )
            return self.formatDT(diplotypeKey), phenotype, interp
        if gene == "SLCO1B1":
            phenotype, interp = self.getInterp(
                self.pgxData["SLCO1B1Interp"], diplotypeKey
            )
            return self.formatDT(diplotypeKey), phenotype, interp
        if gene == "TPMT":
            sa, phenotype = self.pgxData["TPMTDiplotype"][diplotypeKey]
            return sa, phenotype, self.pgxData["TPMTInterp"][phenotype]
        if gene == "DPYD":
            sa, phenotype = self.getInterp(self.pgxData["DPYDDiplotype"], diplotypeKey)
            interp = self.pgxData["DPYDInterp"][phenotype]
            return sa, phenotype, interp
        if gene == "CYP2C19":
            sa, phenotype = self.getInterp(
                self.pgxData["CYP2C19Diplotype"], diplotypeKey
            )
            interp = self.noValNA(self.pgxData["CYP2C19Interp"], phenotype)
            return sa, phenotype, interp
        if gene == "CYP2C9":
            sa, phenotype = self.pgxData["CYP2C9Diplotype"][diplotypeKey]
            interp = self.pgxData["CYP2C9Interp"][phenotype]
            return sa, phenotype, interp
        if gene == "VKORC1":
            phenotype, interp = self.getInterp(
                self.pgxData["VKORC1Interp"], diplotypeKey
            )
            return self.formatDT(diplotypeKey), phenotype, interp
        assert False

    def _parse_line(self, line):
        row = line.strip().split("\t")
        chrom, pos, id, ref, alt, qual, filt, info, format, data = row[0:10]
        genotype = data.split(":")[0]
        assert re.match(r"\d/\d|\.", genotype), genotype
        return chrom, pos, ref, alt, genotype

    def test(self):
        # set the values in results to test all possible modes
        test_cases = pickle.load(open(self.config.get("pgx", "test_cases"), "rb"))
        print("PGX Test cases")
        for t in test_cases:
            results = dict()
            gene, variantData, expectedPhenotype = t
            for v in variantData:
                pos, ref, alt, genotype = v
                pgxr = PgxResult(gene, pos, ref, alt, genotype)
                if gene not in results:
                    results[gene] = list()
                results[gene].append(pgxr)

            sa, phenotype, interp = self.getStarAllele(gene, results[gene])
            assert phenotype == expectedPhenotype
            print(
                "\t".join(
                    (
                        gene,
                        pos,
                        ref,
                        alt,
                        sa,
                        phenotype,
                        str(phenotype == expectedPhenotype),
                    )
                )
            )
        return ["PGX Test Cases", str(len(test_cases)), str(len(test_cases)), "pass"]
