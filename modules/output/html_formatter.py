import time
import qrcode
import base64
import io
import base64
import hashlib
from utils.neptune_util import find_path
from utils.output_util import get_addenda_tag, format_dbutil_date


class HtmlFormatter:
    def __init__(self, config, db_util):
        self.config = config
        self.db_util = db_util
        self.template = config.get("report_template")
        self.references = ""
        for i, ref in enumerate(self.config.get("references")):
            self.references += "{idx}. {ref}<br>".format(idx=str(i + 1), ref=ref)

    def get_filename(self, addenda, corrections, filename_prefix):
        addText = get_addenda_tag(addenda, corrections)
        return filename_prefix.replace("$ADDENDA_TEXT", addText) + self.config.get(
            "html_output_suffix"
        )

    def hash_report_id(self, report_id):
        hasher = hashlib.md5()
        hasher.update(report_id.encode("utf-8"))
        return base64.b64encode(hasher.digest()).decode("utf-8")

    def format_output(self, report_data, sample, filename_prefix):
        report_data.update(
            {"hashedReportID": self.hash_report_id(report_data["reportIdentifier"])}
        )
        for variant in report_data["variants"]:
            variant["variantInterpretation"] = self._wrap_html(
                variant["variantInterpretation"]
            )
        addenda = self.db_util.get_addenda(report_data["localID"])
        corrections = self.db_util.get_corrections(report_data["localID"])
        self._build_addenda(report_data, addenda, corrections)
        d = {
            "cyp2c19SA": self._get_pgx_data(
                report_data["initialPgxData"], "CYP2C19", 0
            ),
            "cyp2c19Pheno": self._get_pgx_data(
                report_data["initialPgxData"], "CYP2C19", 1
            ),
            "dpydSA": self._get_pgx_data(report_data["initialPgxData"], "DPYD", 0),
            "dpydPheno": self._get_pgx_data(report_data["initialPgxData"], "DPYD", 1),
            "cyp2c9SA": self._get_pgx_data(report_data["initialPgxData"], "CYP2C9", 0),
            "cyp2c9Pheno": self._get_pgx_data(
                report_data["initialPgxData"], "CYP2C9", 1
            ),
            "vkorc1SA": self._get_pgx_data(report_data["initialPgxData"], "VKORC1", 0),
            "vkorc1Pheno": self._get_pgx_data(
                report_data["initialPgxData"], "VKORC1", 1
            ),
            "tpmtSA": self._get_pgx_data(report_data["initialPgxData"], "TPMT", 0),
            "tpmtPheno": self._get_pgx_data(report_data["initialPgxData"], "TPMT", 1),
            "ifnl3SA": self._get_pgx_data(report_data["initialPgxData"], "IFNL3", 0),
            "ifnl3Pheno": self._get_pgx_data(report_data["initialPgxData"], "IFNL3", 1),
            "slco1b1SA": self._get_pgx_data(
                report_data["initialPgxData"], "SLCO1B1", 0
            ),
            "slco1b1Pheno": self._get_pgx_data(
                report_data["initialPgxData"], "SLCO1B1", 1
            ),
        }
        pgx_table = (
            self._get_pgx_table(
                report_data["initialPgxData"], report_data["clinicalSite"]
            )
            % d
        )
        pgx_interpretation = self._get_interpretations(report_data["initialPgxData"])
        file_path = self.get_filename(addenda, corrections, filename_prefix)
        with open(file_path, "w") as out:
            with open(self.template) as f:
                for line in f:
                    new_line = self._replace_in_line(
                        line, report_data, pgx_table, pgx_interpretation
                    )
                    out.write("%s\n" % new_line)
        print(file_path)
        return file_path

    def format_phi_output(self, phi_data, filename_prefix):
        phi_fields = [
            "PATIENT_NAME",
            "PATIENT_ID",
            "PATIENT_AGE",
            "PATIENT_DOB",
            "PATIENT_GENDER",
            "PATIENT_SAMPLE_ID",
            "PATIENT_ACCESSION",
            "SAMPLE_COLLECTED_DATE",
            "SAMPLE_RECEIVED_DATE",
            "SAMPLE_TYPE",
            "DISEASE_NAME",
            "PHYSICIAN_NAME",
            "REPORT_DATE",
        ]
        pre_report = find_path(filename_prefix + self.config.get("html_output_suffix"))
        file_path = pre_report.replace("preReport", "toReview")
        with open(file_path, "w") as output, open(pre_report) as input:
            for line in input:
                output.write(
                    "%s\n" % self._replace_in_line_phi(line, phi_data, phi_fields)
                )
        print(file_path)
        return file_path

    def _replace_in_line_phi(self, line, phi_data, phi_fields):
        for field in phi_fields:
            if field in line:
                line = line.replace("<!-- " + field + " -->", phi_data[field])
        return line.strip()

    def _get_pgx_data(self, pgxDict, gene, idx):
        return pgxDict[gene][idx] if gene in pgxDict else ""

    def buildCNVDescription(self, report_data):
        title = """<h3>
                        <span style='font-size:11pt;font-family:Arial;'>
                                Table 2: Details of Copy Number Variants:
                        </span>
                    </h3>"""
        if report_data["cnvsFailed"] == "True":
            return (
                "%s <span style='font-size:11pt;font-family:Arial;'>CNV analysis failed for this sample.</span>"
                % title
            )
        descriptions = list()
        genes = set()
        for variant in report_data["variants"]:
            if variant["type"] == "CNV":
                if variant["gene"] in genes:
                    continue
                genes.add(variant["gene"])
                s = (
                    "<p><span style='font-size:11pt;font-family:Arial;'>%s</span></p>"
                    % (variant["variantInterpretation"])
                )
                descriptions.append(s)
        if len(descriptions) > 0:
            return "%s %s" % (title, "\n".join(descriptions))
        else:
            return (
                "%s <span style='font-size:11pt;font-family:Arial;'>None detected</span>"
                % title
            )

    def buildCNVTable(self, variants):
        header = """<tr style='background:#D9E2F3;'>
                    <th style='border: 1px solid #8EAADB;'>Disease</th> 
                    <th style='border: 1px solid #8EAADB;'>Inh.</th> 
                    <th style='border: 1px solid #8EAADB;'>Gene</th> 
                    <th style='border: 1px solid #8EAADB;'>Variant</th> 
                    <th style='border: 1px solid #8EAADB;'>Zyg.</th> 
                    <th style='border: 1px solid #8EAADB;'>Notes</th> 
                    <th style='border: 1px solid #8EAADB;'>Interpretation</th> 
                 </tr>"""
        rows = list()
        for cnv in variants:
            if cnv["type"] == "CNV":
                cnvVarDesc = "%s\n %s (%s)" % (
                    cnv["variantCuration"],
                    cnv["geneRegion"],
                    cnv["transcript"],
                )
                d = [
                    "<td style='text-align:center; border: 1px solid #8EAADB;'> %s </td>"
                    % s
                    for s in (
                        cnv["disease"],
                        cnv["inheritance"],
                        cnv["gene"],
                        cnvVarDesc,
                        cnv["zygosity"],
                        "",
                        cnv["interpretation"],
                    )
                ]
                rows.append("".join(d))
        if len(rows) == 0:
            return ""
        return (
            "<div align='center'><table style='display:block;page-break-inside:avoid;font-size:12;border-collapse:collapse;'>%s\n <tr>%s</tr></table></div>"
            % (header, "\n</tr><tr>\n".join(rows))
        )

    def buildVariantTable(self, variants):
        title = """<h3>
                        <span style='font-size:11.0pt;font-family:Arial;'>
                                Table 1: Details of Pathogenic and Likely Pathogenic Variants
                        </span>
                    </h3>"""
        header = """<tr style='background:#D9E2F3;'>
                    <th style='border: 1px solid #8EAADB;'>Disease</th> 
                    <th style='border: 1px solid #8EAADB;'>Inh.</th> 
                    <th style='border: 1px solid #8EAADB;'>Gene</th> 
                    <th style='border: 1px solid #8EAADB;'>Position&nbsp;(NCBI&nbsp;37)</th> 
                    <th style='border: 1px solid #8EAADB;'>Variant</th> 
                    <th style='border: 1px solid #8EAADB;'>Zyg.</th> 
                    <th style='border: 1px solid #8EAADB;'>Notes</th> 
                    <th style='border: 1px solid #8EAADB;'>Interpretation</th> 
                 </tr>"""
        rows = list()
        interpCount = 0
        for rd in variants:
            if rd["type"] != "CNV":
                interpCount += 1
                position = "chr%s:%s" % (rd["chromosome"], rd["position"])
                if "genomic" in rd:
                    position = "chr%s %s" % (rd["chromosome"], rd["genomic"])
                variant = rd["cDNA"]
                if len(rd["cDNA"]) > 0 and len(rd["proteinChange"]) > 0:
                    variant += "<BR>"
                variant += rd["proteinChange"]
                d = [
                    "<td style='text-align:center; border: 1px solid #8EAADB;'> %s </td>"
                    % s
                    for s in (
                        rd["disease"],
                        rd["inheritance"],
                        rd["gene"],
                        position,
                        variant,
                        rd["zygosity"],
                        rd["notes"],
                        rd["interpretation"],
                    )
                ]
                rows.append("".join(d))
        if interpCount > 0:
            return (
                "%s<div align='center'><table style='display:block;page-break-inside:avoid;font-size:12;border-collapse:collapse;'>%s\n <tr>%s</tr></table></div>"
                % (title, header, "\n</tr><tr>\n".join(rows))
            )
        else:
            return (
                "%s <span style='font-size:11pt;font-family:Arial;'>None detected</span>"
                % title
            )

    def buildVariantDescriptions(self, report_data):
        descriptions = list()
        title = """<h3>
                        <span style='font-size:11pt;font-family:Arial;'>
                            PATHOGENIC AND/OR LIKELY PATHOGENIC VARIANTS %s
                        </span>
                    </h3>"""
        interpCount = 0
        for variant in report_data["variants"]:
            if variant["type"] != "CNV":
                interpCount += 1
                descriptions.append(variant["variantInterpretation"])
        corr = ""
        if len(report_data["clinicalCorrelation"]) > 0:
            corr = (
                """<p>
                        <span style='font-size:11pt;font-family:Arial;'>
                            %s
                        </span>
                    </p>"""
                % report_data["clinicalCorrelation"]
            )
        if interpCount > 0:
            title = title % ("DETECTED")
            return "%s %s %s" % (title, corr, "\n".join(descriptions))
        else:
            title = title % ("NOT DETECTED")
            return (
                "%s <span style='font-size:11pt;font-family:Arial;'>No variants that meet reporting criteria were identified in this patient's sample.</span>"
                % title
            )

    def getSangerVariantNote(self, confirmed):
        return "The variant was confirmed by Sanger sequencing." if confirmed else ""

    def buildCoverageString(self, excidData):
        return ", ".join(
            ["%s (%s&#37;)" % (gene, coverage) for gene, coverage in excidData]
        )

    def build_qrcode(self, hashed_id):
        buffer = io.BytesIO()
        qr = qrcode.QRCode(version=1, box_size=4, border=1)
        qr.add_data(hashed_id)
        qr.make(fit=True)
        img = qr.make_image()
        img.save(buffer)
        encoded = base64.b64encode(buffer.getvalue()).decode("utf-8")
        return "<img style='width: 60pt;' src='data:image/png;base64," + "%s'/>" % (
            encoded
        )

    def _wrap_addenda(self, addendums):
        return (
            """
        <h3><span style='font-size:11pt;font-family:Arial;'>ADDENDUM</span></h3>
        <p><span style='font-size:11pt;font-family:Arial;'>%s</span></p>"""
            % addendums
        )

    def _format_addenda(self, addendums):
        formattedAddendums = list()
        for add in addendums:
            date, text = add
            formattedAddendums.append(date + "<br>" + text)
        return "<br>".join(formattedAddendums)

    def _build_interpretation(self, interpretations):
        return (
            '<p style="font-size:11pt;font-family:Arial;">'
            + '</p><p style="font-size:11pt;font-family:Arial;">'.join(interpretations)
            + "</p>"
        )

    def _get_pgx_table(self, pgxData, site):
        pgxGeneTemplates = [
            ("CYP2C19", PGX_TABLE_TEMPLATE_CYP2C19),
            ("DPYD", PGX_TABLE_TEMPLATE_DPYD),
            ("IFNL3", PGX_TABLE_TEMPLATE_IFNL3),
            ("SLCO1B1", PGX_TABLE_TEMPLATE_SLCO1B1),
            ("TPMT", PGX_TABLE_TEMPLATE_TPMT),
            ("CYP2C9", PGX_TABLE_TEMPLATE_CYP2C9),
            ("VKORC1", PGX_TABLE_TEMPLATE_VKORC1),
        ]
        results = list()
        for gene, template in pgxGeneTemplates:
            if gene in pgxData:
                results.append(template)
        return "".join(results)

    def _wrap_html(self, text):
        return (
            "<p><span style='font-size:11.0pt;font-family:Arial;'>%s</span></p>" % text
        )

    def _format_comment(self, comment):
        return "".join([self._wrap_html(line) for line in comment])

    def _append_addendum(self, data, results):
        for result in results:
            data["addendums"].append(
                (format_dbutil_date(result["date"]), result["text"])
            )

    def _build_addenda(self, report_data, addenda, corrections):
        report_data["addendums"] = list()
        self._append_addendum(report_data, addenda)
        self._append_addendum(report_data, corrections)

    def _get_interpretations(self, pgx_data):
        interpretations = list()
        pgx_genes = [g.strip() for g in self.config.get("pgx", "genes").split(",")]
        for gene in pgx_genes:
            if gene == "VKORC1":
                interpretations.append(pgx_data["CYP2C9"][2] + pgx_data["VKORC1"][2])
            elif gene != "CYP2C9":  # concat VK + CYP2C9
                interpretations.append(pgx_data[gene][2])
        return interpretations

    def _replace_in_line(self, line, report_data, pgxTable, pgx_interpretation):
        if "HASHED_REPORT_ID" in line:
            line = line.replace(
                "HASHED_REPORT_ID", "##reportID:%s" % report_data["hashedReportID"]
            )
        if "QR_CODE" in line:
            line = line.replace(
                "<!-- QR_CODE -->", self.build_qrcode(report_data["hashedReportID"])
            )
        if "ADDENDUM" in line:
            if "addendums" in report_data and len(report_data["addendums"]):
                addendum = self._wrap_addenda(
                    self._format_addenda(report_data["addendums"])
                )
                line = line.replace("<!-- ADDENDUM -->", addendum)
        if "BACKGROUND" in line:
            line = line.replace("<!-- BACKGROUND -->", self.config.get("background"))
        if "VIP_DATABASE_VERSION" in line:
            line = line.replace("<!-- VIP_DATABASE_VERSION -->", report_data["vipFile"])
        if "NEPTUNE_VERSION" in line:
            line = line.replace(
                "<!-- NEPTUNE_VERSION -->", report_data["neptuneVersion"]
            )
        if "SAMPLE_RECEIVED_DATE" in line:
            line = line.replace(
                "<!-- SAMPLE_RECEIVED_DATE -->", report_data["sampleReceivedDate"]
            )
        if "LOCAL_ID" in line:
            line = line.replace("LOCAL_ID", "##localID:%s" % report_data["localID"])
        if "VARIANT_DESCRIPTION" in line:
            line = line.replace(
                "<!-- VARIANT_DESCRIPTION -->",
                self.buildVariantDescriptions(report_data),
            )
        if "VARIANT_TABLE" in line:
            line = line.replace(
                "<!-- VARIANT_TABLE -->",
                self.buildVariantTable(report_data["variants"]),
            )
        if "CNV_DESCRIPTION" in line:
            line = line.replace(
                "<!-- CNV_DESCRIPTION -->", self.buildCNVDescription(report_data)
            )
        if "CNV_TABLE" in line and report_data["cnvsFailed"] == False:
            line = line.replace(
                "<!-- CNV_TABLE -->", self.buildCNVTable(report_data["variants"])
            )
        if "COVERAGE_STRING" in line:
            line = line.replace(
                "<!-- COVERAGE_STRING -->",
                self.buildCoverageString(report_data["geneCoverage"]),
            )
        if "COMMENT_RECOMMENDATIONS" in line:
            line = line.replace(
                "<!-- COMMENT_RECOMMENDATIONS -->",
                self._format_comment(report_data["overallInterpretationText"]),
            )
        if "<!-- SITE_METHODOLOGY -->" in line:
            line = line.replace(
                "<!-- SITE_METHODOLOGY -->",
                self._wrap_html(self.config.get("clinical_site_methodology")),
            )
        if report_data["isPreliminary"]:
            if "<!-- REPORT_STATUS_TITLE -->" in line:
                line = line.replace("<!-- REPORT_STATUS_TITLE -->", "Preliminary ")
            if "<!-- REPORT_STATUS -->" in line:
                line = line.replace("<!-- REPORT_STATUS -->", "Preliminary Report")
        if "<!-- PGX_TABLE -->" in line:
            line = line.replace("<!-- PGX_TABLE -->", pgxTable)
        if "<!-- PGX_INTERPRETATIONS -->" in line:
            line = line.replace(
                "<!-- PGX_INTERPRETATIONS -->",
                self._build_interpretation(pgx_interpretation),
            )
        if "<!-- REFERENCES -->" in line:
            line = line.replace("<!-- REFERENCES -->", self.references)
        return line.strip()


PGX_TABLE_TEMPLATE_CYP2C19 = """
<tr>
    <td rowspan=4 style='text-align:center; border: 1px solid #8EAADB;'> CYP2C19 </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> clopidogrel </td>
    <td rowspan=4 style='text-align:center; border: 1px solid #8EAADB;'> %(cyp2c19SA)s  </td>
    <td rowspan=4 style='text-align:center; border: 1px solid #8EAADB;'> %(cyp2c19Pheno)s </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-clopidogrel-and-cyp2c19/ </td>
</tr>

<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> voriconazole </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-voriconazole-and-cyp2c19/ </td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> citalopram, escitalopram </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> 
    https://cpicpgx.org/guidelines/guideline-for-selective-serotonin-reuptake-inhibitors-and-cyp2d6-and-cyp2c19/
    </td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> amitriptyline </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> 
    https://cpicpgx.org/guidelines/guideline-for-tricyclic-antidepressants-and-cyp2d6-and-cyp2c19/
    </td>
</tr>
"""
PGX_TABLE_TEMPLATE_DPYD = """
<tr>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> DPYD </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> capecitabine </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> %(dpydSA)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> %(dpydPheno)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-fluoropyrimidines-and-dpyd/ </td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> fluorouracil </td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> tegafur</td>
</tr>
"""
PGX_TABLE_TEMPLATE_IFNL3 = """
<tr>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> IFNL3 </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> peginterferon alfa-2a</td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> rs12979860 %(ifnl3SA)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> %(ifnl3Pheno)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-peg-interferon-alpha-based-regimens-and-ifnl3/ </td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> peginterferon alfa-2b</td>
</tr>
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> ribavirin</td>
</tr>
"""
PGX_TABLE_TEMPLATE_SLCO1B1 = """
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> SLCO1B1 </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> simvastatin</td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> rs4149056 %(slco1b1SA)s </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> %(slco1b1Pheno)s </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-simvastatin-and-slco1b1/ </td>
</tr>
"""
PGX_TABLE_TEMPLATE_TPMT = """
<tr>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> TPMT </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> azathioprine </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> %(tpmtSA)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> %(tpmtPheno)s </td>
    <td rowspan=3 style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-thiopurines-and-tpmt/ </td>
</tr>

<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> mercaptopurine </td>
</tr>

<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> thioguanine </td>
</tr>

"""
PGX_TABLE_TEMPLATE_CYP2C9 = """
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> CYP2C9 </td>
    <td rowspan=2 style='text-align:center; border: 1px solid #8EAADB;'> warfarin </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> %(cyp2c9SA)s </td>
    <td rowspan=2 style='text-align:center; border: 1px solid #8EAADB;'> %(cyp2c9Pheno)s </td>
    <td rowspan=2 style='text-align:center; border: 1px solid #8EAADB;'> https://cpicpgx.org/guidelines/guideline-for-warfarin-and-cyp2c9-and-vkorc1/ </td>
</tr>"""
PGX_TABLE_TEMPLATE_VKORC1 = """
<tr>
    <td style='text-align:center; border: 1px solid #8EAADB;'> VKORC1 </td>
    <td style='text-align:center; border: 1px solid #8EAADB;'> %(vkorc1SA)s </td>
</tr>
"""
