import datetime
import csv
import re
import pickle
import time
import json
from utils.neptune_util import parse_gene_list, find_path, run_cmd
from utils.output_util import write_json, get_addenda_tag, format_dbutil_date


class FhirFormatter:
    def __init__(self, config, db_util):
        self.config = config
        self.hgnc_map = self.load_hgnc_map(config.get("hgnc_list"))
        self.db_util = db_util

    def format_output(self, report_data, sample, filename_prefix):
        self.build_methodology(report_data, self._get_methodology())
        addenda = self.db_util.get_addenda(sample.internal_id)
        corrections = self.db_util.get_corrections(sample.internal_id)
        self._build_addenda(report_data, addenda, corrections)
        self.build_snv_indel_variants(report_data)
        self.build_cnv_variants(report_data)
        report_data["overallInterpretationText"] = self.format_comment(
            report_data["overallInterpretationText"]
        )
        self.add_fhir_data(report_data, sample)
        self.build_pgx_data(report_data)
        file_path = self.get_filename(addenda, corrections, filename_prefix)
        write_json(file_path, report_data)
        self.write_final_fhir(file_path)
        return file_path

    def format_phi_output(self, phi_data, filename_prefix):
        pre_report = find_path(filename_prefix + self.config.get('fhir_preprocessed_output_suffix'))
        with open(pre_report) as f:
            report_data = json.load(f)
        file_path = pre_report.replace("preReport", "toReview")
        report_data.update(
            {
                "patientName": phi_data["PATIENT_NAME"],
                "dateOfBirth": phi_data["PATIENT_DOB"],
                "age": phi_data["PATIENT_AGE"],
                "sex": phi_data["PATIENT_GENDER"],
                "patientSampleID": phi_data["PATIENT_SAMPLE_ID"],
                "localID": phi_data["PATIENT_ACCESSION"],
                "reportDate": phi_data["REPORT_DATE"],
                "sampleCollectedDate": phi_data["SAMPLE_COLLECTED_DATE"],
                "specimenType": phi_data["SAMPLE_TYPE"],
                "indicationForTesting": phi_data["DISEASE_NAME"],
            }
        )
        write_json(file_path, report_data)
        self.write_final_fhir(file_path)
        return file_path

    def write_final_fhir(self, preprocessed_file):
        final_file = preprocessed_file.replace(self.config.get('fhir_preprocessed_output_suffix'), self.config.get('fhir_final_output_suffix'))
        run_cmd('java -jar {jar} {preprocessed_file} {final_file}'.format(
            jar=self.config.get('fhir_jar'),
            preprocessed_file=preprocessed_file,
            final_file=final_file))
        return final_file

    def load_hgnc_map(self, hgnc_list):
        hgnc_map = dict()
        with open(hgnc_list) as f:
            reader = csv.reader(f)
            for row in reader:
                gene, hgnc_id = row
                hgnc_map[gene] = hgnc_id
        return hgnc_map

    def format_comment(self, comment):
        return " ".join(comment)

    def get_filename(self, addenda, corrections, filename_prefix):
        addenda_tag = get_addenda_tag(addenda, corrections)
        return filename_prefix.replace("$ADDENDA_TEXT", addenda_tag) + self.config.get(
            'fhir_preprocessed_output_suffix'
        )

    def _get_methodology(self):
        return self.config.get("clinical_site_methodology")

    def build_methodology(self, report_data, site_methodology):
        methodology = (
            self.config.get("methodology")
            .replace("$VERSION", report_data["neptuneVersion"])
            .replace("$VIP_VERSION", report_data["vipFile"])
        )
        report_data.update(
            {
                "siteMethodology": [site_methodology],
                "methodology": [
                    methodology,
                    site_methodology,
                    self.config.get("vip_note"),
                ],
            }
        )

    def _get_gene_coverage_text(self, excid_data):
        if len(excid_data) == 0:
            return "All genes have 100% of targeted bases sequenced to redundant coverage of 20x or greater."
        gct = list()
        for ed in excid_data:
            gct.append("{0} ({1}%)".format(ed["gene"], ed["coverage"]))
        return (
            "All genes have 100%% of targeted bases sequenced to redundant coverage of 20x or greater with the following exceptions: %s. Further information, including specific coverage for this patient's sample, is available in the ExCID report."
            % (", ".join(gct))
        )

    def add_fhir_data(self, data, sample):
        data.update(
            {
                "testName": self.config.get('test_name'),
                "testCode": sample.get_sampling_lab() + self.config.get('test_code'),
                "caseType": self.config.get('case_type'),
                "testInfoSummary": self.config.get('background'),
                "testInfoDetail": self.config.get('background'),
                "vusDisclaimer": self.config.get('vus_disclaimer'),
                "pgxDisclaimer": self.config.get('pgx_disclaimer'),
                "panelFinding": self._get_panel_finding(data["overallInterpretation"]),
                "panelSummary": "",
                "familyScreening": "",
                "panelManagementRecommendation": "",
                "patientSampleID": sample.get_external_id(),
                "references": self.config.get("references"),
                "footer": self.config.get("footer"),
                "comments": "",
                "panelRecommendationDetail": data["overallInterpretationText"],
                "humanReferenceSequenceAssemblyVersion": self.config.get('human_ref_assembly_version'),
                "genomicCoordinateSystem": self.config.get('genomic_coordinate_system'),
                "orderingPhysicianNPI": "",
                "language": self.config.get('language'),
                "dnaVariationCode": "",
                "panelRecommendation": data["overallInterpretationText"],
                "practitionerData": self.config.get('practitioners'),
                "pgxSummaryRecommendation": list(),
                "deidentified": False,
                "familyID": "",
                "familyRelationship": ""
            }
        )
        if sample.cnv_analysis_failed:
            data["cnvsFailedText"] = self.config.get('cnv_failed_description')
        data["pgxFinding"] = ""
        if data["overallInterpretation"] == "Negative":
            data["noReportableVariantsText"] = self.config.get('no_variants_description')
        data["jsonCreatedDate"] = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S")
        excid_data = list()
        for gene, pct in data["geneCoverage"]:
            excid_data.append({"gene": gene, "coverage": pct})
        data["geneCoverage"] = excid_data
        data["geneCoverageText"] = self._get_gene_coverage_text(excid_data)

    def build_snv_indel_variants(self, report_data):
        for variant in report_data["variants"]:
            if variant["type"] == "SNV/InDel":
                variant.update(
                    {
                        "notes": self._clean_line_breaks(variant["notes"], "; "),
                        "chromosome": "Chromosome " + variant["chromosome"],
                        "inheritance": self.expand_inheritance(variant["inheritance"]),
                        "dbSNPID": "",
                        "isIncidentalFinding": "",
                        "hgncID": self.hgnc_map[variant["gene"]],
                        "reportable": variant["interpretation"]
                        != "Uncertain Significance",
                        "proteinChange": ""
                        if variant["proteinChange"].lower() == "n/a"
                        else variant["proteinChange"],
                        "variantCuration": self._clean_line_breaks(
                            variant["variantCuration"], " "
                        ),
                        "proteinChangeEffect": self._clean_line_breaks(
                            variant["proteinChangeEffect"], "; "
                        ),
                        "variantInterpretation": self._clean_line_breaks(
                            variant["variantInterpretation"], " "
                        ),
                    }
                )
            self._format_diseases(variant)
            del variant["disease"]

    def _append_addendum(self, data, results):
        for result in results:
            data["addendums"].append(
                {
                    "date": format_dbutil_date(result["date"]),
                    "reportChangeSummary": result["text"],
                    "reportChangeDetail": result["text_detail"],
                }
            )

    def _build_addenda(self, report_data, addenda, corrections):
        report_data["addendums"] = list()
        self._append_addendum(report_data, addenda)
        self._append_addendum(report_data, corrections)
        if len(report_data["addendums"]) > 0:
            report_data["reportStatus"] = "Corrected"

    def build_cnv_variants(self, report_data):
        for variant in report_data["variants"]:
            if variant["type"] == "CNV":
                variant["notes"] = ""
                variant["reportable"] = (
                    variant["interpretation"] != "Uncertain Significance"
                )

    def build_pgx_data(self, report_data):
        if "initialPgxData" in report_data:
            drug_recommendations = self._get_drug_recommendations()
            report_data["pgxData"] = list()
            for gene in report_data["initialPgxData"]:
                diplotype, phenotype, interp = report_data["initialPgxData"][gene]
                report_data["pgxData"].append(
                    {
                        "geneSymbol": gene,
                        "diplotype": diplotype,
                        "phenotype": phenotype,
                        "interpretation": interp,
                        "pgxDrugRecommendation": drug_recommendations[gene],
                        "recommendation": "",
                        "summary": interp,
                        "hgncID": self.hgnc_map[gene],
                    }
                )
            del report_data["initialPgxData"]

    def _get_drug_recommendations(self):
        return pickle.load(open(self.config.get("pgx_drug_recommendations"), "rb"))

    def _format_diseases(self, variant):
        diseases = list()
        for s in variant['diseases']:
            split = s.strip().split(" [")
            diseases.append(
                {
                    "text": split[0],
                    "code": split[1]
                    .replace("]", "")
                    .replace("MIM ", "")
                    .replace("MIM:", ""),
                    "ontology": "https://omim.org/",
                }
            )
            variant['diseases'] = diseases

    def _clean_line_breaks(self, string, delimiter):
        string = re.compile("<br>", re.IGNORECASE).split(string)
        whitespace_removed = [i.strip() for i in string if i != "" and not i.isspace()]
        return delimiter.join(whitespace_removed)

    def expand_inheritance(self, inheritance):
        if inheritance == "AD":
            return "Autosomal dominant"
        elif inheritance == "AR":
            return "Autosomal recessive"
        elif inheritance == "AD/AR":
            return "Autosomal dominant / Autosomal recessive"
        return inheritance

    def _get_panel_finding(self, finding):
        return finding.upper()
