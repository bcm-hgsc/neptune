import re
import json
from utils.neptune_util import find_path
from utils.output_util import write_json, get_addenda_tag, format_dbutil_date


class DefaultFormatter:
    def __init__(self, config, db_util):
        self.config = config
        self.db_util = db_util

    def format_output(self, report_data, sample, filename_prefix):
        report_data.update(
            {
                "accessionNumber": sample.internal_id,
                "background": self.config.get("background"),
            }
        )
        self.build_methodology(report_data, self.getMethod())
        addenda = self.db_util.get_addenda(sample.internal_id)
        corrections = self.db_util.get_corrections(sample.internal_id)
        self._build_addenda(report_data, addenda, corrections)
        self._build_pgx_data(report_data)
        file_path = self.get_filename(addenda, corrections, filename_prefix)
        write_json(file_path, report_data)
        return file_path

    def format_phi_output(self, phi_data, filename_prefix):
        pre_report = find_path(
            filename_prefix + self.config.get("default_output_suffix")
        )
        with open(pre_report) as f:
            report_data = json.load(f)
        file_path = pre_report.replace("preReport", "toReview")
        report_data.update(
            {
                "patientName": phi_data["PATIENT_NAME"],
                "dateOfBirth": phi_data["PATIENT_DOB"],
                "age": phi_data["PATIENT_AGE"],
                "sex": phi_data["PATIENT_GENDER"],
                "patientSampleID": phi_data["PATIENT_SAMPLE_ID"],
                "localID": phi_data["PATIENT_ACCESSION"],
                "reportDate": phi_data["REPORT_DATE"],
                "sampleCollectedDate": phi_data["SAMPLE_COLLECTED_DATE"],
                "specimenType": phi_data["SAMPLE_TYPE"],
                "indicationForTesting": phi_data["DISEASE_NAME"],
                "orderingPhysicianName": phi_data["PHYSICIAN_NAME"],
            }
        )
        write_json(file_path, report_data)
        return file_path

    def get_filename(self, addenda, corrections, filename_prefix):
        addText = get_addenda_tag(addenda, corrections)
        return filename_prefix.replace("$ADDENDA_TEXT", addText) + self.config.get(
            "default_output_suffix"
        )

    def wrap_html(self, text):
        return (
            "<p><span style='font-size:11.0pt;font-family:Arial;'>%s</span></p>" % text
        )

    def getMethod(self):
        return self.wrap_html(self.config.get("clinical_site_methodology"))

    def build_methodology(self, report_data, site_methodology):
        methodology = (
            self.config.get("methodology")
            .replace("$VERSION", report_data["neptuneVersion"])
            .replace("$VIP_VERSION", report_data["vipFile"])
        )
        report_data.update(
            {
                "siteMethodology": site_methodology,
                "methodology": methodology
                + site_methodology
                + self.config.get("vip_note"),
            }
        )

    def _append_addendum(self, data, results):
        for result in results:
            data["addendums"].append(
                (format_dbutil_date(result["date"]), result["text"])
            )

    def _build_addenda(self, report_data, addenda, corrections):
        report_data["addendums"] = list()
        self._append_addendum(report_data, addenda)
        self._append_addendum(report_data, corrections)
        if len(report_data["addendums"]) > 0:
            report_data["reportStatus"] = "AMENDMENT_FINAL"

    def _build_pgx_data(self, report_data):
        if "initialPgxData" in report_data:
            report_data["pgxData"] = list()
            for gene in report_data["initialPgxData"]:
                diplotype, phenotype, interp = report_data["initialPgxData"][gene]
                drugs = self.config.get("json_pgx_recommendations", gene, "drugs")
                recomm = self.config.get(
                    "json_pgx_recommendations", gene, "recommendations"
                )
                report_data["pgxData"].append(
                    {
                        "geneSymbol": gene,
                        "diplotype": diplotype,
                        "phenotype": phenotype,
                        "interpretation": interp,
                        "drugs": drugs,
                        "recommendation": recomm,
                    }
                )
            del report_data["initialPgxData"]
