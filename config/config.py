from collections import OrderedDict
from pathlib import Path
import yaml
from utils.neptune_util import get_logger

PUBLIC_RELEASE_VERSION = '1.0'
NEPTUNE_BASE_PATH = str(Path(__file__).absolute().parent.parent)
GLOBAL_CONFIG_PATH = str(Path(NEPTUNE_BASE_PATH, "config", "global_config.yaml"))
CONFIGS_PATH = str(Path(NEPTUNE_BASE_PATH, "config"))
PROJECT_CONFIGS = OrderedDict(
    (('default', 'project_config.yaml'), ('create-your-own', 'new_project_config.yaml'))
)
OUTPUT_TYPES = OrderedDict(
    (
        ("default", "output/output_default.yaml"),
        ("html", "output/output_html.yaml"),
        ("fhir", "output/output_fhir.yaml"),
    )
)


class Config:
    __slots__ = ("_attr",)

    def __init__(self):
        self._attr = dict()
        self.load_config(Path(GLOBAL_CONFIG_PATH).absolute())

    def load_config(self, path):
        data = None
        with open(str(path), "r") as fh:
            data = yaml.safe_load(fh)
        self._attr.update(data)

    def to_config(self):
        return self._attr

    def get(self, *ref_itr):
        entity = self._attr
        for ref in ref_itr:
            if ref in entity:
                entity = entity[ref]
            else:
                raise ConfigurationKeyError(
                    "No key {key} in configuration".format(
                        key=".".join(map(str, ref_itr))
                    )
                )
        return entity

    def load_project_config(self, project):
        config_path = Path(CONFIGS_PATH, PROJECT_CONFIGS[project]).absolute()
        get_logger().debug("Loading project config file: %s", str(config_path))
        self.load_config(config_path)

    def load_output_types_config(self, output_types):
        for output_type in output_types:
            config_path = Path(CONFIGS_PATH, OUTPUT_TYPES[output_type]).absolute()
            get_logger().debug("Loading output config file(s): %s", str(config_path))
            self.load_config(config_path)


class ConfigurationKeyError(Exception):
    """
    Invalid configuration key
    """
