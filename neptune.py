"""
Neptune: An environment for the delivery of genomic medicine
"""

import argparse
import sys
from pathlib import Path
from collections import OrderedDict
from utils.neptune_util import find_path
from config.config import PUBLIC_RELEASE_VERSION, Config
from modules.module_loader import ModuleLoader
from command.annotate import Annotate
from command.render_pre_report import RenderPreReport
from command.render_final_report import RenderFinalReport
from command.reanalyze import Reanalyze
from command.correct import Correct
from command.validate import Validate

__version__ = PUBLIC_RELEASE_VERSION


class Neptune:
    def __init__(self):
        self.module_loader = ModuleLoader()
        self.commands = OrderedDict((
            ('annotate', Annotate),
            ('render-prereport', RenderPreReport),
            ('render-final-report', RenderFinalReport),
            ('reanalyze', Reanalyze),
            ('correct', Correct),
            ('validate', Validate),
        ))

    def main(self):
        args = self._setup_args()
        config = Config()
        command = self.commands[args.command]
        command(args.options, config, self.module_loader).run()

    def _setup_args(self):
        cmd_str = '\n'.join([
            '{cmd_name} => {cmd_doc}'.format(
                cmd_name=cmd_name,
                cmd_doc=cmd_cls.__doc__.strip(),
            )
            for cmd_name, cmd_cls in self.commands.items()
            if cmd_cls.__doc__ is not None
        ])
        usage = 'Neptune public release version {version}\n{docstr}\n\n' \
                'available commands:\n{cmd_str}'.format(
                    version=PUBLIC_RELEASE_VERSION,
                    docstr=__doc__.strip(),
                    cmd_str=cmd_str
                )
        parser = argparse.ArgumentParser(
            description=usage,
            formatter_class=argparse.RawDescriptionHelpFormatter
        )
        parser.add_argument(
            'command',
            choices=[
                'annotate',
                'render-prereport',
                'render-final-report',
                'reanalyze',
                'correct',
                'validate',
            ],
            metavar='COMMAND',
            help='Name of the command to execute.',
        )
        parser.add_argument(
            'options', metavar='OPTIONS',
            help='Options to pass to the command. Run \'python neptune.py COMMAND -h\' '
                'to see all avaiable options for the command.',
            nargs=argparse.REMAINDER
        )
        return parser.parse_args()

if __name__ == "__main__":
    Neptune().main()
