from utils.neptune_util import makedir
import xml.etree.ElementTree
import datetime
import glob
import os
from utils.neptune_util import parse_vcf_info, get_classification, find_path


class Sample:
    def __init__(self, info_file, flowcell_id, config):
        info = self._load(info_file, flowcell_id)
        self.config = config
        self.fcID = flowcell_id
        self.lane_barcode = info.get("ID")
        self.internal_id = info.get("Sample")
        self.cnv_analysis_failed = True  # the CNV analysis failed for this sample
        self.fc_lane_barcode = self.get_flowcell_lane_barcode()
        self.excid_path = self._expand_dir(self.config.get("excid_path"))
        self.mab_path = self._expand_dir(self.config.get("mab_path"))
        self.cardio_risk_path = self._expand_dir(self.config.get("cardio_risk_path"))
        self.output_dir = self._setup_output_dir()
        self.metadata = SampleMetadata(
            self.internal_id,
            info.get("SampleReceiveDate"),
            info.get("sampling_lab"),
            config.get("test", "sample_info"),
        )

    def __str__(self):
        return self.internal_id

    def get_pgx_path(self):
        return self._expand_dir(self.config.get("pgx", "pgx_path"))

    def get_cnv_path(self):
        return self._expand_dir(self.config.get("cnv_data_file"))

    def get_snp_gvcf(self):
        return self._expand_dir(self.config.get("snp_gvcf"))

    def _load(self, info_file, flowcell_id):
        e = xml.etree.ElementTree.parse(info_file).getroot()
        self.batchID = e.get("BatchId")
        return e.find("LaneBarcodeInfo").find("LaneBarcode")

    def _get_sample_path(self):
        sample_path = self.config.get("sample_path") % (
            self.fcID,
            self.lane_barcode,
        )
        return find_path(sample_path)

    def get_vip_annotation(self):
        return self._expand_dir(self.config.get("vip_annotated_path"))

    def get_vip_variants(self):
        vip_variants = list()
        vip_annotation = self.get_vip_annotation()
        with open(vip_annotation) as f:
            for line in f:
                if line.startswith("#"):
                    continue
                fields = line.strip().split("\t")
                chrom, pos = fields[:2]
                ref, alt, qual, filt, info = fields[3:8]
                data = fields[9]
                variant = SampleVariant(
                    self.internal_id, chrom, pos, ref, alt, qual, filt, info, data
                )
                vip_variants.append(variant)
        return vip_variants

    def get_snp_f5_annotation(self):
        return self._expand_dir(self.config.get("snp_f5_annotated_path"), zero_ok=True)

    def get_snp_annotation(self):
        return self._expand_dir(self.config.get("snp_annotated_path"))

    def get_flowcell_lane_barcode(self):
        return "-".join([self.fcID, self.lane_barcode])

    def set_cnv_analysis_failed(self, sampleCNVsFailed):
        self.cnv_analysis_failed = sampleCNVsFailed

    def _setup_output_dir(self):
        output_dir = os.path.join(self._get_sample_path(), "neptune")
        makedir(output_dir)
        return output_dir

    def _expand_dir(self, path_pattern, zero_ok=False):
        return find_path(self._get_sample_path() + "/" + path_pattern, zero_ok)

    def get_sampling_lab(self):
        return self.metadata.sampling_lab

    def get_received_date(self):
        return self.metadata.received_date

    def get_patient_id(self):
        return self.metadata.patient_id

    def get_external_id(self):
        return self.metadata.external_id


class SampleMetadata:
    def __init__(self, internal_id, received_date, sampling_lab, metadata_file):
        self.received_date = self._get_received_date(received_date)
        self.external_id, self.patient_id = self._get_sample_info(
            internal_id, metadata_file
        )
        self.sampling_lab = sampling_lab

    def __str__(self):
        return "\t".join([self.patient_id, self.sampling_lab])

    def _get_sample_info(self, internal_id, metadata_file):
        with open(metadata_file) as f:
            for line in f:
                line_fields = line.split("\t")
                if line_fields[0] == internal_id:
                    external_id = line_fields[-1][:-1]
                    patient_id = line_fields[-2]
                    return external_id, patient_id

    def _get_received_date(self, received_date):
        return datetime.datetime.strptime(received_date, "%Y-%m-%d").strftime(
            "%m/%d/%Y"
        )


class SampleVariant:
    def __init__(
        self, sampleID, chrom, pos, ref, alt, qual, filt, info, data, zygosity=None
    ):
        self.sampleID = sampleID
        self.chrom = chrom
        self.pos = pos
        self.ref = ref
        self.alt = alt
        self.qual = qual
        self.filt = filt
        self.info = info
        self.data = data
        self.zygosity = zygosity

    def __str__(self):
        return "%s\n" % "\t".join(
            (
                self.chrom,
                self.pos,
                ".",
                self.ref,
                self.alt,
                self.qual,
                self.filt,
                self.info,
                "GT:VR:RR:DP:GQ",
                self.data,
            )
        )

    def getGenotype(self):
        if self.data.startswith("1/1"):
            return "%s/%s" % (self.alt, self.alt)
        if self.data.startswith("0/1"):
            return "%s/%s" % (self.ref, self.alt)
        if self.data.startswith("1/0"):
            return "%s/%s" % (self.ref, self.alt)
        if self.data.startswith("0/0"):
            return "%s/%s" % (self.ref, self.ref)
        if self.data.startswith("."):
            return "None"
        assert False, self.data

    def getAF(self):
        row = self.data.split(":")
        return "%s / %s" % (row[1], row[3])

    def getGene(self):
        return parse_vcf_info(self.info)["gene"]

    def get_classification(self, info=None):
        if not info:
            info = self.info
        return get_classification(info)

    def key(self):
        return "%s|%s|%s|%s" % (self.chrom, self.pos, self.ref, self.alt)

    def chromPos(self):
        return "%s|%s" % (self.chrom, self.pos)

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        if self.chrom == other.chrom:
            return self.pos < other.pos
        return self.chrom < other.chrom

    def __eq__(self, other):
        return (
            self.sampleID == other.sampleID
            and self.chrom == other.chrom
            and self.pos == other.pos
            and self.ref == other.ref
            and self.alt == other.alt
            and self.qual == other.qual
            and self.filt == other.filt
            and sorted(self.info.split(";")) == sorted(other.info.split(";"))
            and self.data == other.data
        )

    def __hash__(self):
        return hash(
            (
                self.sampleID,
                self.chrom,
                self.pos,
                self.ref,
                self.alt,
                self.info,
                self.data,
            )
        )
