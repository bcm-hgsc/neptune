import csv
import itertools
import re
import time
import datetime
import uuid
from utils.neptune_util import parse_vcf_info
from modules.module_loader import ModuleLoader
from utils.output_util import clean_text, get_inheritance_pattern, build_comment


class DataBuilder(object):

    def __init__(self, config, dbUtil, deidentify):
        self.config = config
        self.dbUtil = dbUtil
        self.deidentify = deidentify
        module_loader = ModuleLoader()
        self.report_features = list()
        for report_feature in self.config.get('report_feature_modules'):
            reporter = module_loader.load_module(report_feature['module'])
            self.report_features.append(reporter(self.config, report_feature['config']))

    def _load_gene_data(self):
        geneData = dict() # key is gene name, value is omim / swissprot pair
        with open(self.config.get("genes", "curated_gene_info")) as f:
            reader = csv.reader(f)
            for row in reader:
                #Gene,Original List,Report?,Actionable?,inheritance,OMIM/Baylor,Reporting Baylor
                gene, transcript, originalList, report, actionable, inherit, disease, diseaseDescription = row
                actionable = True
                if gene == "Gene": continue
                geneData[gene] = (diseaseDescription.strip("\""), disease.strip("\""), inherit, actionable)
        return geneData

    def confirm_allowed_diseases(self, diseases, data):
        for d in diseases:
            assert d in self.config.get("allowed_diseases"), d
        data["diseases"] = list(diseases)

    def _build_description(self, variant):
        gene = variant["gene"]
        tx = variant["transcript"] if "transcript" in variant else ""
        if len(tx) > 0:
            gene = "%s (%s)" % (gene, tx)
        protein_change = ""
        if len(variant["proteinChange"]) > 0:
            protein_change = " (%s)" % variant["proteinChange"]
        sangerConfirmationString = ""
        if variant["confirmedBySanger"] and variant["sangerRequired"]:
            sangerConfirmationString = ", which was confirmed by Sanger sequencing"
        s = "%s %s%s %s variant in the %s gene was detected in this individual%s. %s\
            %s" % (self._wrap_zyg(variant["zygosity"].lower()), variant["cDNA"], protein_change, variant["interpretation"].lower(), gene, sangerConfirmationString, variant["geneDiseaseText"], variant["variantCuration"])
        return s.strip()

    def _wrap_zyg(self, zyg):
        if zyg.lower() == "homozygous":
            return "An apparently homozygous"
        return "A %s" % zyg

    def add_disease(self, variant, diseases_set):
        variant["diseases"] = list()
        if len(variant["disease"]) > 0:
            diseaseSplit = re.split(r'(?<=\]),|;', variant["disease"])
            variant["diseases"] = [s.strip() for s in diseaseSplit]
            for d in diseaseSplit:
                diseases_set.add(d.strip())

    def formatInterpretation(self, path):
        path = path.upper()
        if path == "PATH" or path == "PATHOGENIC":
            return "Pathogenic"
        elif path == "LPATH":
            return "Likely pathogenic"
        elif path == "RISK ALLELE":
            return "Risk factor"
        elif path == "VUS":
            return "Uncertain significance"

    def formatCategoryType(self, interpretation):
        if interpretation == "Uncertain Significance":
            return "UNKNOWN_SIGNIFICANCE"
        return "MEDICALLY_SIGNIFICANT"

    def formatVIPFile(self, vipDatabase):
        return ".".join(vipDatabase.split(".")[-3:-1])

    def get_excid_data(self, excid_path):
        excid_data = list()
        with open(excid_path) as f:
            for line in f:
                # ACTA2   NM_001613(100.000)  100.00  HGMD
                gene, transcript, pct = line.strip().split("\t")[0:3]
                if float(pct.replace("%", "")) < 100.0:
                    excid_data.append((gene, pct))
        return excid_data

    def deidentify_report(self, report_data):
        report_data.update({
            "sampleReceivedDate": "",
            "jsonCreatedDate": "",
            "patientID": str(uuid.uuid4()),
            "reportIdentifier": report_data["reportIdentifier"].split("-")[-1],
            "deidentified": True
        })

    def initialize_data(self, snv_variants, codex_data, sample):
        sanger_confirmed = self.dbUtil.get_confirmed_variants(sample)
        is_positive = self._get_finding(snv_variants)
        is_preliminary = is_positive and len(sanger_confirmed) == 0
        gene_data = self._load_gene_data()
        data = {
            "reportIdentifier": self._get_report_id(
                [sample.get_patient_id(), sample.get_external_id(), sample.internal_id]
            ),
            "localID": sample.internal_id,
            "patientID": sample.get_patient_id(),
            "vipFile": self.formatVIPFile(self.config.get('vip_file')),
            "sangerSiteNote": "",
            "clinicalCorrelation": "",
            "geneCoverage": self.get_excid_data(sample.excid_path),
            "neptuneVersion": self.config.get("version"),
            "clinicalSite": sample.get_sampling_lab(),
            "sampleReceivedDate": sample.get_received_date(),
            "isPreliminary": is_preliminary,
            "reportStatus": "Preliminary" if is_preliminary else "Final",
            "overallInterpretationText": build_comment(is_positive, is_preliminary, self.config),
            "overallInterpretation": "Positive" if is_positive else "Negative"
        }
        if "variants" not in data:
            data["variants"] = list()
        diseases = set()
        for sv in snv_variants:
            variant = self.buildVariant(sv, gene_data, sanger_confirmed, codex_data)
            self.add_disease(variant, diseases)
            data["variants"].append(variant)
        self.confirm_allowed_diseases(diseases, data)
        for feature in self.report_features:
            feature.add_data(data, sample, gene_data=gene_data)
        if self.deidentify:
            self.deidentify_report(data)
        return data

    def _get_finding(self, snv_variants):
        return len(snv_variants) > self._count_vus_snvs(snv_variants)

    def _count_vus_snvs(self, snv_variants):
        count = 0
        if snv_variants:
            for snv in snv_variants:
                if snv.get_classification() == "VUS":
                    count += 1
        return count

    def _get_report_id(self, id_values):
        id_values.append(str(time.time()))
        return "-".join(id_values)
    
    def buildVariant(self, sv, geneData, sangerConfirmed, codex_data):
        variant = dict()
        # Everything in the sanger confirmed hashtable has been "confirmed". 
        # however, not everything in the table required sanger for confirmation. the value of sangerRequired represents this distinction
        infoFields = parse_vcf_info(sv.info)
        confirmed = sv.chromPos() in sangerConfirmed
        notes = ""
        if "comment" in infoFields:
            notes = infoFields["comment"]
        if confirmed and variant["sangerRequired"]:
            notes += "<br>Confirmed by Sanger sequencing"
        variant["notes"] = clean_text(notes)

        gene = infoFields["gene"]
        variant["gene"] = gene
        path, variantInterpretation = "", ""
        if "emerge_category" in infoFields:
            path = infoFields["emerge_category"]
        elif "classificationbcm" in infoFields: # SNP column TODO merge
            path = infoFields["classificationbcm"]
        variant.update({
            "type": "SNV/InDel",
            "chromosome": sv.chrom,
            "position": sv.pos,
            "ref": sv.ref,
            "alt": sv.alt,
            "externalId": "%s,%s,%s,%s" % (sv.pos, sv.chrom, sv.ref, sv.alt),
            "confirmedBySanger": confirmed,
            "sangerRequired": sangerConfirmed[sv.chromPos()] if confirmed else True,
            "genotype": sv.getGenotype(),
            "alleleFraction": sv.getAF(),
            "notInterpreted": False,
            "interrogatedButNotFound": False,
            "interpretation": self.formatInterpretation(path),
            "zygosity": codex_data[sv.key()]
        })
        variant["categoryType"] = self.formatCategoryType(variant["interpretation"])
        print(infoFields)
        if "emerge_interpretation_report" in infoFields:
            print("Variant Interpretation", infoFields["emerge_interpretation_report"])
            variantInterpretation = infoFields["emerge_interpretation_report"]
            variantInterpretation = re.sub(r'\[\d\d\d\d-\d\d-\d\d\]', '', variantInterpretation)

        variant["variantCuration"] = clean_text(variantInterpretation)
        variant["proteinChangeEffect"] = self.buildEffect(infoFields, True)
        variant["proteinChange"] = ""
        if "aminoacid" in infoFields:
            variant["proteinChange"] = infoFields["aminoacid"]
        elif "aachange" in infoFields:
            variant["proteinChange"] = infoFields["aachange"]

        variant["cDNA"] = ""
        if "nucleotide_corrected" in infoFields:
            variant["cDNA"] = infoFields["nucleotide_corrected"].strip()
            variant["dnaChange"] = infoFields["nucleotide_corrected"].strip()

        variant["transcript"] = infoFields["transcript_used"] if "transcript_used" in infoFields else ""
        if "genomic" in infoFields:
            variant["genomic"] = infoFields["genomic"]

        inheir, disease, diseaseText = "", "", "Defects in this gene cause no known diseases."
        if gene in geneData:
            diseaseText, disease, inheir, actionable = geneData[gene] 
            diseaseText = diseaseText.strip()
            if len(diseaseText) > 0 and not diseaseText.endswith("."):
                diseaseText += "."
        else:
            if "baylorinterpretationdisorder" in infoFields:
                diseaseText = infoFields["baylorinterpretationdisorder"]
            if "omim" in infoFields:
                disease = infoFields["omim"]
            if "inheritance" in infoFields:
                inheir = infoFields["inheritance"]

        variant["geneDiseaseText"] = clean_text(diseaseText)
        variant["inheritance"] = inheir
        variant["disease"] = disease
        # TODO move this into the sample variant object; remove duplication between gene & snp info
        variant["variantInterpretation"] = " ".join([self._build_description(variant), get_inheritance_pattern(variant["inheritance"], self.config)])
        return variant

    def buildEffect(self, infoFields, isTable):
        aa = infoFields["aminoacid"] if "aminoacid" in infoFields else ""
        nc = infoFields["nucleotide_corrected"] if "nucleotide_corrected" in infoFields else ""
        tx = infoFields["transcript_used"] if "transcript_used" in infoFields else ""
        if isTable:
            if len(aa) > 0 and len(nc) > 0 and len(tx) > 0:
                return "%s (%s) <br> (%s)" % (nc, aa, tx)
        elif len(aa) > 0 and len(nc) > 0:
            return "%s (%s)" % (nc, aa)
        return " ".join((nc, aa, tx))
