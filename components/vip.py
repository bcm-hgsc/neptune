import os
import re
from itertools import dropwhile
from utils.neptune_util import get_key, get_classification
from components.annotation_sources import ClinVar


class VIP:
    def __init__(self, vip_file, config):
        self.config = config
        self.clinvar = ClinVar(self.config.get("clinvar"))
        self.path_vip_variants = dict()  # key is chr|pos|ref|var, value is info
        self.vus_variants = dict()
        with open(vip_file) as f:
            for line in dropwhile(lambda x: x.startswith("#"), f):
                # CHROM	POS	    ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	SAMPLE
                (
                    chrom,
                    pos,
                    vcfid,
                    ref,
                    alt,
                    qual,
                    filt,
                    info,
                    dataFormat,
                    sample,
                ) = line.strip().split("\t")
                self._parse_line(chrom, pos, ref, alt, info)

    def _parse_line(self, chrom, pos, ref, alt, info):
        path = get_classification(info)
        if path in ["PATH", "LPATH", "REVIEW", "HARMONIZATION", "RISK ALLELE"]:
            self.path_vip_variants[get_key(chrom, pos, ref, alt)] = info
        elif path == "VUS_START":
            self._add_vus_variants(chrom, pos, ref, alt, info, self.vus_variants)

    def _add_vus_variants(self, chrom, pos, ref, alt, info, vus_list):
        key = get_key(chrom, pos, ref, alt)
        if self.clinvar.hasEntry(chrom, pos, ref, alt):
            clinvar_info = self.clinvar.get(key)[7]
            clinvar_dict = self._parse_clinvar_info(clinvar_info)
            clinvar_cat = clinvar_dict["CLNSIG"]
            if self._has_path(clinvar_cat):
                vus_list[key] = info

    def _parse_clinvar_info(self, clinvar_info):
        result = dict()
        for entry in clinvar_info.split(";"):
            if len(entry.split("=")) == 2:
                key, value = entry.split("=")
                result[key] = value
            else:
                result[entry] = True
        return result

    def _has_path(self, clinvar_cat):
        result = False
        for a in clinvar_cat.split("|"):
            for b in a.split(","):
                assert len(b) == 1 or b == "255", "unexpected clinvar category %s" % b
                if b == "4" or b == "5":
                    result = True
                    break
        return result
