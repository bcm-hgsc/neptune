from utils.neptune_util import get_key


def hash_vcf(vcfFile):
    # CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
    result = dict()
    with open(vcfFile) as f:
        for line in f:
            if line.startswith("#"):
                continue
            if len(line.strip()) == 0:
                continue
            row = line.strip().split("\t")
            chrom, pos, id, ref, alt, qual, filter = row[0:7]
            for mult_alt in alt.split(","):
                info = "\t".join(row[7:])
                result[get_key(chrom, pos, ref, mult_alt)] = [
                    chrom,
                    pos,
                    id,
                    ref,
                    alt,
                    qual,
                    filter,
                    info,
                ]
    return result


class ClinVar:
    def __init__(self, path):
        self.clinvar = hash_vcf(path)

    def keys(self):
        return list(self.clinvar.keys())

    def get(self, key):
        return self.clinvar[key]

    def hasEntry(self, chrom, pos, ref, alt):
        key = get_key(chrom, pos, ref, alt)
        return key in self.clinvar
