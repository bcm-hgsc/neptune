# Neptune: An environment for the delivery of genomic medicine
In support of the goal of delivering genomic data to the EHR, we have developed Neptune, an environment that enables users to identify and report known disease-causing variants in gene sets of interest, to gather curations for potential novel pathogenic variants from an external review system, and to share and update structured genomic knowledge with EHR systems and collaborators.

The key features of Neptune are:
1. to take as input genomic data (genotypes and coverage information) and compare against a ‘VIP database’ of known genetic variation, marking known variants with previously-curated data and selecting novel genomic variants for review
2. to combine data from diverse sources including sample metadata from a LIMS and variant information from the VIP database and output data in a structured report file ready to be accepted by EHR systems
3. to convert that structured data into a customizable human-readable report
4. to enable corrected and updated reports
5. to enable the reanalysis and re-interpretation of data over time

## To apply the Neptune environment before use:
1. Download and install miniconda (https://docs.conda.io/en/latest/miniconda.html#)
2. Navigate to your Neptune directory and run: `conda env create -f environment.yml`
3. Activate the environment: `conda activate neptune`

## To produce FHIR reports:
Note: Java 11 is required.
1. Create your account at https://loinc.org/ in order to fetch LOINC code mappings to be used in FHIR generation.
2. Add your LOINC credentials in `utils/fhir-client/src/main/resources/application.properties`.
3. Compile the FHIR client jar: `mvn clean install -f utils/fhir-client/`
4. Refer to help or see below for example commands to produce reports in FHIR format.
The Java program included is a modification of https://github.com/emerge-ehri/FHIRGenomicsImplementation. A future Neptune update will allow PDF and ExCID attachments in the FHIR bundle.

## Usage
For help, run:

`python neptune.py -h`

`python neptune.py annotate -h`

`python neptune.py render-prereport -h`

`python neptune.py render-final-report -h`

`python neptune.py reanalyze -h`

`python neptune.py correct -h`

Customize your reports by updating .yaml files in `/config`.\
When adding a new .yaml for a new type of project, map the new file path to a keyword arg in `/config/config.py/PROJECT_CONFIGS`.

## Command examples
**Run the Help commands provided in the Usage section above for details on the options.**
### Annotate
Annotates VCF using data from the VIP; i.e., marks if a variant has been previously seen or needs review.\
`python neptune.py annotate -p default -i 100204 -f HMCYNBCXX`
### RenderPreReport
Loads data from external sources and creates structured output files.\
`python neptune.py render-prereport -p default -i 100204 -f HMCYNBCXX -t default html fhir`
### RenderFinalReport
Populates the pre-report with the final set of data. This command is separate from the RenderPreReport command, in case this step needs to run in a PHI environment.\
`python neptune.py render-final-report -p default -i 100204 -f HMCYNBCXX -t html fhir default -r data/validation/test_requisition.csv`
### Reanalyze
Takes an existing VIP-annotated VCF and tags the differences compared to the current VIP.\
`python neptune.py reanalyze -v data/vip/2020-09-03.vip.txt`
### Correct
Adds report correction note to the Neptune sqlite3 database.\
`python neptune.py correct -f HMCYNBCXX -i 100204 -r 'This is a correction note.' -d 'This is a detailed explanation of the correction.'`
### Validate
Runs Annotate and RenderPreReport commands on example samples; compares output to a previously saved set of passing reports.\
`python neptune.py validate`

The Validate command is to 1) demonstrate example outputs, 2) test if Neptune is in working order, and 3) highlight differences in VIP-annotated VCFs and pre-reports after making changes in Neptune. Currently, the report comparisons will be evaluated as failures; this is because 'reportIdentifier' and/or 'hashedReportID' fields will not match those in the saved set of reports due to varying real-time timestamps added during report generation. Ensure that the only differences you see are these timestamps and hashes. This issue will be addressed in a future update.
#### An example of failing report comparisons:
```
Jsons do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12878_HMCYNBCXX-1-IDMB1/neptune/NA12878.100204.NEG.preReport.1-IDMB1.default.json, data/validation/HMCYNBCXX/truthsets/jsons/NA12878.100204.NEG.preReport.1-IDMB1.default.json
- 'reportIdentifier', 'NA12878-siteSampleID-100204')
+ 'reportIdentifier', 'NA12878-siteSampleID-100204-1604011251.694357')
?                                                 ++++++++++++++++++

Jsons do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12878_HMCYNBCXX-1-IDMB1/neptune/NA12878.100204.NEG.preReport.1-IDMB1.FHIR.json, data/validation/HMCYNBCXX/truthsets/jsons/NA12878.100204.NEG.preReport.1-IDMB1.FHIR.json
- 'jsonCreatedDate', 'jsonCreatedDate')
+ 'jsonCreatedDate', '2020-10-29 17:40:52')
- 'reportIdentifier', 'NA12878-siteSampleID-100204')
+ 'reportIdentifier', 'NA12878-siteSampleID-100204-1604011251.694357')
?                                                 ++++++++++++++++++

Jsons do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12805_HMCYNBCXX-1-IDMB76/neptune/NA12805.100060.NEG.preReport.1-IDMB76.default.json, data/validation/HMCYNBCXX/truthsets/jsons/NA12805.100060.NEG.preReport.1-IDMB76.default.json
- 'reportIdentifier', 'NA12805-siteSampleID-100060')
+ 'reportIdentifier', 'NA12805-siteSampleID-100060-1604011252.603675')
?                                                 ++++++++++++++++++

Jsons do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12805_HMCYNBCXX-1-IDMB76/neptune/NA12805.100060.NEG.preReport.1-IDMB76.FHIR.json, data/validation/HMCYNBCXX/truthsets/jsons/NA12805.100060.NEG.preReport.1-IDMB76.FHIR.json
- 'jsonCreatedDate', 'jsonCreatedDate')
+ 'jsonCreatedDate', '2020-10-29 17:40:53')
- 'reportIdentifier', 'NA12805-siteSampleID-100060')
+ 'reportIdentifier', 'NA12805-siteSampleID-100060-1604011252.603675')
?                                                 ++++++++++++++++++

Json comparison complete: 4 incorrect
Files do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12878_HMCYNBCXX-1-IDMB1/neptune/NA12878.100204.NEG.preReport.1-IDMB1.html, data/validation/HMCYNBCXX/truthsets/htmls/NA12878.100204.NEG.preReport.1-IDMB1.html
- <!-- ##reportID:76+fQEMCVYVpJX3JtO8tfA== -->

+ <!-- ##reportID:EL7syeOTJHdTxLvp9oOykg== -->

- <img style='width: 60pt;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsAQAAAACnHWpgAAAA4ElEQVR4nKWVwW0DQQwDRwf/qQ7cf1nXAVUB83CAxEmAsxz9CJDgrsTVVvhWc/BUF3Cqiuquqaor8jO8EQarRJid9mCKYTxMXZP/gOL+OvkHbI3f0SpA546y1JYbHp4NXmlJEqMkSXbaY6ZKUExt+4ylKBjE0vems4cyZLbZwELGCdreF0cGAdr2+aCROBOItmcOxEGAt74jjSYkuW9zpekM0KTeylXiWNppyw0pUkbL+d4IM2KsuST/6rOxPwe8fQsAyAPxctc94Mio8Rt7Qwadrxp9lQFbitEyz/WPf+EDp52DPhgj+coAAAAASUVORK5CYII='/>

+ <img style='width: 60pt;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsAQAAAACnHWpgAAAA2klEQVR4nKWVwY0DUQhDH1HupoPpv6zpwFTgPeSyUSLNMsvNkp/4HwlT4VfNg7e6kFNVQ09NVV2Z3+WTMJqRCbNjH0xBM83UtflTjkX+av6Qpu+wCtAyypItNxiABu/6KgkNrSS7vjg2Vgzyri8BxQq2tGUVIAFnyT5mJqEnYpZzJpItxcD2zVZk28RZz8pGRiTJmkVIluQta3Di1yrs2Aowh8uaS/O33GCgB7Tdo8TS2IK+kRtzNlTjG7svLOFjmXUv2T2d41z/VwGBWc/5SYHd9LnO2PrHXfgBLPKGLt8mKwQAAAAASUVORK5CYII='/>

Files do NOT match for data/validation/HMCYNBCXX/project/Sample_NA12805_HMCYNBCXX-1-IDMB76/neptune/NA12805.100060.NEG.preReport.1-IDMB76.html, data/validation/HMCYNBCXX/truthsets/htmls/NA12805.100060.NEG.preReport.1-IDMB76.html
- <!-- ##reportID:y8pywPevghsW7fiYjwwDew== -->

+ <!-- ##reportID:vnxTFM6qT6UN24LkVIJIZA== -->

- <img style='width: 60pt;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsAQAAAACnHWpgAAAA1klEQVR4nKWVsQ1DQQxCn0/p8QZ//7GyAZ6AtIlS/Fhxh2QkOIOuwtvM4WNu4FTVTDdTVXfLn/BBGJpAmB33MAV2DVP3y99QSP3r8rtmYJrMnntQUFRCWXLLDQagwSsuSRJQkmTHPUNVkaFnlvc9TGQwbrTjYhlZQZKXfg2xFKPtWx2kuTCYrDWDFINsrTUbFDBe+40UOxLrbNjBIGCtOUls2THrXFUVPMWQdfcTw9UtetkjbOwQwFvNALRbiGUXsHGsxGifycDFNJq1X8CKibc9qj/+hRfFMIrunqH69wAAAABJRU5ErkJggg=='/>

+ <img style='width: 60pt;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsAQAAAACnHWpgAAAA3UlEQVR4nKWVu3FEMQwDl28uhzq4/st6HQAVwIETf4Izz8wwwx1RJERN+RK5+BYvZGYm5ySZmVfJ3+WDEplpSnbsRQbOGDKvk3/L3OX8NfmnfML9DqtCANQl+2DADjDgFUvbmtZtu2RdGyqBvD3XqiojrHXNGCgty5ofDMJEI8+STYmOgPup3YywLOoaa9vnVi6IantfWsuoar31hoUry9C3fOVCreV8r8wMog542+fWtU299pWNLWzYvoULQLGg2133KX1ydLz01QM1PKHhLNmLmZNbILY7dv7xL3wACdOYJfew/tUAAAAASUVORK5CYII='/>

File comparisons completed: 2 incorrect
________________________________________
           Validation File Comparisons
Files	# Pass	# Missing	# Total	Result
________________________________________
VIP annotations	2	0	2	pass
JSONs	0	0	4	FAIL
HTMLs	0	0	2	FAIL
```
